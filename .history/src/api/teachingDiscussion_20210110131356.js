import request from '@/utils/request'
import store from '@/store/index'

export function addplans(plans) {
  return request({
    url: '/plans/one',
    method: 'post',
    params: {
      plansPeopleId: plans.plansPeopleId,
      plansStudentId: plans.plansStudentId, // 学生姓名
      plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      plansLessonPlan: plans.plansLessonPlan, // 课程计划
      plansStageGoal: plans.plansStageGoal, // 阶段目标
      plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      plansTeacherId: store.getters.userId
      // plansId: plans.plansId // 计划id

    }
  })
}

export function adddiscuss(teachingDiscussion, BB) {
  console.log(BB)
  return request({
    url: '/teachingDiscussion/insert',
    method: 'post',
    params: {
      teachingDiscussionId: teachingDiscussion.teachingDiscussionId,
      teachingDiscussionEventType: teachingDiscussion.teachingDiscussionEventType,
      teachingDiscussionEventTitle: teachingDiscussion.teachingDiscussionEventTitle,
      teachingDiscussionEventDescription: teachingDiscussion.teachingDiscussionEventDescription,
      teachingDiscussionParticipants: teachingDiscussion.teachingDiscussionParticipants,
      teachingDiscussionSolution: teachingDiscussion.teachingDiscussionSolution,
      teachingDiscussionExecutionRecord: teachingDiscussion.teachingDiscussionExecutionRecord,
      teachingDiscussionEventSummary: teachingDiscussion.teachingDiscussionEventSummary,
      teachingDiscussionStatus: teachingDiscussion.teachingDiscussionStatus,

      teachingDiscussionPrincipalTeacherId: teachingDiscussion.teachingDiscussionPrincipalTeacherId,
      teachingDiscussionTeacherId: teachingDiscussion.teachingDiscussionTeacherId,
      teachingDiscussionTeachingSupervisorId: teachingDiscussion.teachingDiscussionTeachingSupervisorId,
      teachingDiscussionClazzTypeId: teachingDiscussion.teachingDiscussionClazzTypeId,
      teachingDiscussionTechnicalTypeId: teachingDiscussion.teachingDiscussionTechnicalTypeId,
      teachingDiscussionPostUserId: BB
    }
  })
}

export function getAll() { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all',
    method: 'get'
  })
}

export function getAllteachingDiscussion(BB) { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachingDiscussionByDirector(BB) { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachingDiscussionAAA() { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all',
    method: 'get'

  })
}

export function getAllteachingDiscussionByprincipal(BB) { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}
export function getteachingDiscussionById(id) { // 根据id获取信息
  return request({
    url: '/teachingDiscussion/one',
    method: 'get',
    params: {
      id: id
    }
  })
}
export function getclazzplansById(id) {
  return request({
    url: 'plansClazz/one',
    method: 'get',
    params: {
      Id: id // 根据计划id获取班级计划
    }
  })
}
export function updateplans(plans) {
  return request({
    url: '/plans/',
    method: 'put',
    params: {
      plansPeopleId: plans.plansPeopleId,
      plansStudentId: plans.plansStudentId, // 学生姓名
      plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      plansLessonPlan: plans.plansLessonPlan, // 课程计划
      plansStageGoal: plans.plansStageGoal, // 阶段目标
      plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      plansTeacherId: store.getters.userId,
      plansId: plans.plansId // 计划id
    }
  })
}

export function updateplansByprincipal(teachingDiscussion) {
  return request({
    url: '/teachingDiscussion/update',
    method: 'post',
    params: {
      teachingDiscussionId: teachingDiscussion.teachingDiscussionId,
      teachingDiscussionEventType: teachingDiscussion.teachingDiscussionEventType,
      teachingDiscussionEventTitle: teachingDiscussion.teachingDiscussionEventTitle,
      teachingDiscussionEventDescription: teachingDiscussion.teachingDiscussionEventDescription,
      teachingDiscussionParticipants: teachingDiscussion.teachingDiscussionParticipants,
      teachingDiscussionSolution: teachingDiscussion.teachingDiscussionSolution,
      teachingDiscussionExecutionRecord: teachingDiscussion.teachingDiscussionExecutionRecord,
      teachingDiscussionEventSummary: teachingDiscussion.teachingDiscussionEventSummary,
      teachingDiscussionStatus: teachingDiscussion.teachingDiscussionStatus,
      teachingDiscussionPostUserId: teachingDiscussion.teachingDiscussionPostUserId,
      teachingDiscussionPrincipalTeacherId: teachingDiscussion.teachingDiscussionPrincipalTeacherId,
      teachingDiscussionTeacherId: teachingDiscussion.teachingDiscussionTeacherId,
      teachingDiscussionTeachingSupervisorId: teachingDiscussion.teachingDiscussionTeachingSupervisorId,
      teachingDiscussionClazzTypeId: teachingDiscussion.teachingDiscussionClazzTypeId,
      teachingDiscussionTechnicalTypeId: teachingDiscussion.teachingDiscussionTechnicalTypeId
    }
  })
}

export function updateclazzplans(plans) { // 根据班级计划id修改
  return request({
    url: '/plansClazz/',
    method: 'put',
    params: {
      plansclazzStageGoal: plans.plansclazzStageGoal, // 阶段目标
      plansclazzId: plans.plansclazzId, // 班级计划id
      plansclazzTrainingPrograms: plans.plansclazzTrainingPrograms, // 训练项目
      plansclazzTeachingPreparation: plans.plansclazzTeachingPreparation, // 教学准备
      plansclazzMattersNeedingAttention: plans.plansclazzMattersNeedingAttention // 注意事项
      // 注意事项
    }
  })
}

export function deleteplans(id) {
  return request({
    url: '/plans/',
    method: 'delete',
    params: {
      plansId: id
    }
  })
}

export function getAllTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}

export function selectclazzType() {
  return request({
    url: '/clazzType/all',
    method: 'get'
  })
}

export function selectTechnicalType() {
  return request({
    url: '/technicalType/all',
    method: 'get'
  })
}
