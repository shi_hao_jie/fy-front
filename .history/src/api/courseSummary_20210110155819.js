import request from '@/utils/request'

export function addcourseSummer(courseSummary) {
  return request({
    url: '/courseSummary/insert',
    method: 'post',
    params: {

      courseSummaryImplementation: courseSummary.courseSummaryImplementation, // 执行情况
      courseSummaryCourseEvaluation: courseSummary.courseSummaryCourseEvaluation, // 课程评价
      courseSummaryStudentEvaluation: courseSummary.courseSummaryStudentEvaluation // 学生评价
    }
  })
}

export function getAll() { // 获取所有课程总结
  return request({
    url: '/courseSummary/all',
    method: 'get'
  })
}

export function getAllcourseSummary(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllcourseSummaryByDirector(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}
export function getAllcourseSummaryByMedical(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/MedicalDirectorIdJurisdictionZero',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllcourseSummaryByprincipal(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllcourseSummaryAAA() { // 获取所有课程总结
  return request({
    url: '/courseSummary/all',
    method: 'get'

  })
}

export function getcourseSummarybyId(id) {
  return request({
    url: 'courseSummary/one',
    method: 'get',
    params: {
      id: id // 根据计划id获取教学总结
    }
  })
}

export function updatecourseSummaryId(courseSummary) {
  return request({
    url: '/courseSummary/update',
    method: 'post',
    params: {
      courseSummaryId: courseSummary.courseSummaryId, // 课程总结id
      courseSummaryImplementation: courseSummary.courseSummaryImplementation, // 执行情况
      courseSummaryCourseEvaluation: courseSummary.courseSummaryCourseEvaluation, // 课程评价
      courseSummaryStudentEvaluation: courseSummary.courseSummaryStudentEvaluation // 学生评价

    }
  })
}

export function deleteplan(id) {
  return request({
    url: '/plan/',
    method: 'delete',
    params: {
      planId: id
    }
  })
}
