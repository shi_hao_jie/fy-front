import request from '@/utils/request'

export function addStaff(staff) {
  return request({
    url: '/staff/one',
    method: 'post',
    data: {
      staffPwd: 123456,
      staffName: staff.staffName,
      staffSex: staff.staffSex,
      staffBirth: staff.staffBirth,
      staffCall: staff.staffCall,
      staffEmail: staff.staffEmail,
      staffIdcard: staff.staffIdcard,
      staffNation: staff.staffNation,
      staffPolitics: staff.staffPolitics,
      staffMarry: staff.staffMarry,
      staffResidence: staff.staffResidence,
      staffLive: staff.staffLive,
      staffEdu: staff.staffEdu,
      staffSchool: staff.staffSchool,
      staffMajor: staff.staffMajor,
      staffFamily: staff.staffFamily,
      staffContact: staff.staffContact,
      staffGraduationtime: staff.staffGraduationtime,
      staffJobresume: staff.staffJobresume,
      staffHiredate: staff.staffHiredate,
      staffDuty: staff.staffDuty,
      staffTitle: staff.staffTitle,
      staffRank: staff.staffRank,
      staffProfession: staff.staffProfession,
      staffTraining: staff.staffTraining,
      staffWorksummary: staff.staffWorksummary,
      staffDevelop: staff.staffDevelop,
      staffJurisdiction: staff.staffJurisdiction,
      staffSpousesName: staff.staffSpousesName,
      staffSpousesIdcard: staff.staffSpousesIdcard,
      staffSpousesPhone: staff.staffSpousesPhone,
      staffFatherName: staff.staffFatherName,
      staffFatherIdcard: staff.staffFatherIdcard,
      staffFatherPhone: staff.staffFatherPhone,
      staffMotherName: staff.staffMotherName,
      staffMotherIdcard: staff.staffMotherIdcard,
      staffMotherPhone: staff.staffMotherPhone
    }
  })
}

export function getAllStaff() {
  return request({
    url: '/staff/all',
    method: 'get'
  })
}

export function getStaffById(id) {
  return request({
    url: '/staff/id',
    method: 'get',
    params: {
      staffId: id
    }
  })
}

export function updateStaff(staff) {
  return request({
    url: '/staff/update',
    method: 'post',
    params: {
      staffId: staff.staffId,
      staffPwd: staff.staffPwd,
      staffName: staff.staffName,
      staffSex: staff.staffSex,
      staffBirth: staff.staffBirth,
      staffCall: staff.staffCall,
      staffEmail: staff.staffEmail,
      staffIdcard: staff.staffIdcard,
      staffNation: staff.staffNation,
      staffPolitics: staff.staffPolitics,
      staffMarry: staff.staffMarry,
      staffResidence: staff.staffResidence,
      staffLive: staff.staffLive,
      staffEdu: staff.staffEdu,
      staffSchool: staff.staffSchool,
      staffMajor: staff.staffMajor,
      staffFamily: staff.staffFamily,
      staffContact: staff.staffContact,
      staffGraduationtime: staff.staffGraduationtime,
      staffJobresume: staff.staffJobresume,
      staffHiredate: staff.staffHiredate,
      staffDuty: staff.staffDuty,
      staffTitle: staff.staffTitle,
      staffRank: staff.staffRank,
      staffProfession: staff.staffProfession,
      staffTraining: staff.staffTraining,
      staffWorksummary: staff.staffWorksummary,
      staffDevelop: staff.staffDevelop,
      staffJurisdiction: staff.staffJurisdiction,
      staffSpousesName: staff.staffSpousesName,
      staffSpousesIdcard: staff.staffSpousesIdcard,
      staffSpousesPhone: staff.staffSpousesPhone,
      staffFatherName: staff.staffFatherName,
      staffFatherIdcard: staff.staffFatherIdcard,
      staffFatherPhone: staff.staffFatherPhone,
      staffMotherName: staff.staffMotherName,
      staffMotherIdcard: staff.staffMotherIdcard,
      staffMotherPhone: staff.staffMotherPhone
    }
  })
}

export function deleteStaff(id) {
  return request({
    url: '/staff/id',
    method: 'delete',
    params: {
      staffId: id
    }
  })
}

export function selectstaff() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get',
    date: {
    }
  })
}
export function selectstaffPrincipal() {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: 7
    }
  })
}

export function selectstaffSupervisor() {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: 6
    }
  })
}
export function selectstaffTeacher() {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: 8
    }
  })
}
export function getStaffStringD() {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: '6'
    }

  })
}

export function getStaffStringR() {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: '7'
    }

  })
}

export function getStaffStringT() {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: '8'
    }

  })
}

export function getStaffStringM() {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: '1'
    }

  })
}

export function getAllStaffString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'

  })
}
