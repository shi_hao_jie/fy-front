import request from '@/utils/request'
import store from '@/store/index'

export function updateteachsummary(summary) {
  return request({
    url: '/teachsummary/',
    method: 'put',
    params: {
      plansTeacherId: store.getters.userId,
      teachStudentEvaluation: summary.teachStudentEvaluation, // 学生评价
      teachEvaluation: summary.teachEvaluation, // 教学评价
      teachId: summary.teachId, // 教学总结id
      teachClazzId: summary.teachClazzId,
      teachStuid: summary.teachStuid,
      teachPostUserId: summary.teachPostUserId
    }
  })
}

export function updateteachsummaryA(summary, BB) {
  return request({
    url: '/teachsummary/update/replyPrincipalTeacher',
    method: 'post',
    params: {

      staffId: BB,
      teachStudentEvaluation: summary.teachStudentEvaluation, // 学生评价
      teachEvaluation: summary.teachEvaluation, // 教学评价
      teachId: summary.teachId, // 教学总结id
      teachClazzId: summary.teachClazzId,
      teachStuid: summary.teachStuid,
      teachPostUserId: summary.teachPostUserId,
      teachReplyTeachingDirectorContent: summary.teachReplyTeachingDirectorContent
    }
  })
}

export function updateteachsummaryB(summary) {
  return request({
    url: '/teachsummary/',
    method: 'put',
    params: {
      plansTeacherId: store.getters.userId,
      teachStudentEvaluation: summary.teachStudentEvaluation, // 学生评价
      teachEvaluation: summary.teachEvaluation, // 教学评价
      teachId: summary.teachId, // 教学总结id
      teachClazzId: summary.teachClazzId,
      teachStuid: summary.teachStuid,
      teachPostUserId: summary.teachPostUserId,
      teachStatus: 2
    }
  })
}
export function deleteteachsummary(id) {
  return request({
    url: '/teachsummary/',
    method: 'delete',
    params: {
      teachSummaryId: id
    }
  })
}

export function getAllteachsummary(BB) {
  return request({
    url: '/teachsummary/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryByMedical(BB) {
  return request({
    url: '/teachsummary/all/MedicalDirectorIdJurisdictionZero',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryByDirector(BB) {
  return request({
    url: '/teachsummary/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryByprincipal(BB) {
  return request({
    url: '/teachsummary/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryAA() {
  return request({
    url: '/teachsummary/all',
    method: 'get'

  })
}

export function upateStatus(aaa) {
  console.log(aaa)
  return request({
    url: '/teachsummary/status',
    method: 'put',
    params: {
      teachId: aaa.teachId,
      status: 3
    }
  })
}
export function upateStatusA(aaa) {
  return request({
    url: '/teachsummary/status',
    method: 'put',
    params: {
      teachId: aaa.teachId,
      status: 1
    }
  })
}

export function upateStatusB(aaa) {
  return request({
    url: '/teachsummary/status',
    method: 'put',
    params: {
      teachId: aaa.teachId,
      status: 2
    }
  })
}
export function selectclazz() {
  return request({
    url: '/clazz/all',
    method: 'get'

  })
}

export function getteachsummarybyId(id) {
  return request({
    url: '/teachsummary/studentId/all',
    method: 'get',
    params: {
      studentId: id
    }
  })
}

export function addoneteachsummary(summary) {
  return request({
    url: 'teachsummary/insert',
    method: 'post',
    data: {
      teachEvaluation: summary.teachEvaluation,
      teachStudentEvaluation: summary.teachStudentEvaluation,
      teachClazzId: summary.teachClazzId,
      teachStuid: summary.teachStuid,
      teachPostUserId: summary.teachPostUserId,
      teachStatus: 0
    }
  })
}

// export function getAllstudentName() {
//   return request({
//     url: "/student/selectStudentNameAndAdd",
//     method: "get"
//   });
// }
