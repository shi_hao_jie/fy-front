import request from '@/utils/request'
import store from '@/store/index'

export function addplans(plans) {
  return request({
    url: '/plans/one',
    method: 'post',
    params: {
      plansPeopleId: plans.plansPeopleId,
      plansStudentId: plans.plansStudentId, // 学生姓名
      plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      plansLessonPlan: plans.plansLessonPlan, // 课程计划
      plansStageGoal: plans.plansStageGoal, // 阶段目标
      plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      plansStudentId: plans.plansStudentId,
      plansClazzId: plans.plansClazzId,
      plansPeopleResponsibleTeacherId: plans.plansPeopleResponsibleTeacherId,
      plansPeopleTeacherId: plans.plansPeopleTeacherId

    }
  })
}

export function getAllplansByTeacher(BB) { // 学员计划
  return request({
    url: '/plans/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllplans() { // 学员计划
  return request({
    url: '/plans/all',
    method: 'get'
  })
}

export function getAllplansA() { // 学员计划
  return request({
    url: '/plans/all',
    method: 'get'

  })
}

export function getAllclazzplans(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllplansByDirector(BB) { // 计划
  return request({
    url: '/plans/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllplansByprincipal(BB) { // 计划
  return request({
    url: '/plans/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}
export function getAllplansByMedicalID(BB) { // 计划
  return request({
    url: '/plans/all/MedicalDirectorIdJurisdictionZero',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllcp() { // 班级计划
  return request({
    url: '/plansClazz/all',
    method: 'get'
  })
}

export function getAllclazzplansByMedical(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllclazzplansByDirector(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllclazzplansByprincipal(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllclazzplansAAA() { // 班级计划
  return request({
    url: '/plansClazz/all',
    method: 'get'

  })
}

export function getplansById(id) {
  return request({
    url: '/plans/one',
    method: 'get',
    params: {
      plansId: id
    }
  })
}
export function getclazzplansById(id) {
  return request({
    url: 'plansClazz/one',
    method: 'get',
    params: {
      Id: id // 根据计划id获取班级计划
    }
  })
}
export function updateplans(plans) {
  return request({
    url: '/plans/',
    method: 'put',
    params: {
      plansPeopleId: plans.plansPeopleId,
      plansStudentId: plans.plansStudentId, // 学生姓名
      plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      plansLessonPlan: plans.plansLessonPlan, // 课程计划
      plansStageGoal: plans.plansStageGoal, // 阶段目标
      plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      plansTeacherId: store.getters.userId,
      plansId: plans.plansId // 计划id
    }
  })
}

export function updateclazzplans(plans) { // 根据班级计划id修改
  return request({
    url: '/plansClazz/',
    method: 'put',
    params: {
      plansclazzStageGoal: plans.plansclazzStageGoal, // 阶段目标
      plansclazzId: plans.plansclazzId, // 班级计划id
      plansclazzTrainingPrograms: plans.plansclazzTrainingPrograms, // 训练项目
      plansclazzTeachingPreparation: plans.plansclazzTeachingPreparation, // 教学准备
      plansclazzMattersNeedingAttention: plans.plansclazzMattersNeedingAttention // 注意事项
      // 注意事项
    }
  })
}

export function deleteplans(id) {
  return request({
    url: '/plans/',
    method: 'delete',
    params: {
      plansId: id
    }
  })
}

export function getAllTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}
