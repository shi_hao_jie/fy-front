import request from '@/utils/request'

export function updateRecords(records) {
  return request({
    url: '/record/',
    method: 'put',
    params: {
      recordId: records.recordId, // 听课记录id
      recordStudentId: records.recordStudentId, // 学生id
      recordClassroomId: records.recordClassroomId, // 教室id
      recordGuildsuggestion: records.recordGuildsuggestion, // 指导建议
      recordLessonId: records.recordLessonId, // 课程id
      recordListenTimeToString: records.recordListenTimeToString, // 听课日期
      recordMainteacherId: records.recordMainteacherId, // 主课教师id
      recordSuperviseteacherId: records.recordSuperviseteacherId, // 督导教师id
      recordTeachadvantage: records.recordTeachadvantage, // 教学优点
      recordTeachcontent: records.recordTeachcontent, // 教学内容记录
      recordTeachquestion: records.recordTeachquestion // 教学问题
    }
  })
}
export function updatesupervisor(instructionalSupervision) {
  return request({
    url: '/instructionalSupervision/update',
    method: 'post',
    params: {
      instructionalSupervisionEventType: instructionalSupervision.instructionalSupervisionEventType,
      instructionalSupervisionEventTitle: instructionalSupervision.instructionalSupervisionEventTitle,
      instructionalSupervisionEventDescription: instructionalSupervision.instructionalSupervisionEventDescription,
      instructionalSupervisionParticipants: instructionalSupervision.instructionalSupervisionParticipants,
      instructionalSupervisionSolution: instructionalSupervision.instructionalSupervisionSolution,
      instructionalSupervisionExecutionRecord: instructionalSupervision.instructionalSupervisionExecutionRecord,
      instructionalSupervisionEventSummary: instructionalSupervision.instructionalSupervisionEventSummary,
      instructionalSupervisionStatus: instructionalSupervision.instructionalSupervisionStatus,
      instructionalSupervisionId: instructionalSupervision.instructionalSupervisionId
    }
  })
}
export function deleteRecords(id) {
  return request({
    url: '/record/',
    method: 'delete',
    params: {
      recordId: id
    }
  })
}

export function getAll() {
  return request({
    url: 'instructionalSupervision/all',
    method: 'get'
  })
}

export function getAllRecords(BB) {
  return request({
    url: '/instructionalSupervision/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllRecordsA(BB) {
  return request({
    url: '/instructionalSupervision/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllRecordsByprincipal(BB) {
  return request({
    url: '/instructionalSupervision/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllRecordsAAAA() {
  return request({
    url: '/instructionalSupervision/all',
    method: 'get'

  })
}
export function getRecordsbyId(id) {
  return request({
    url: '/instructionalSupervision/one',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function addoneRecord(records) {
  return request({
    url: '/record/one',
    method: 'post',
    data: {
      recordId: records.recordId, // 听课记录id
      recordStudentId: records.recordStudentId, // 学生id
      recordClassroomId: records.recordClassroomId, // 教室id
      recordGuildsuggestion: records.recordGuildsuggestion, // 指导建议
      recordLessonId: records.recordLessonId, // 课程id
      recordMainteacherId: records.recordMainteacherId, // 主课教师id
      recordSuperviseteacherId: records.recordSuperviseteacherId, // 督导教师id
      recordTeachadvantage: records.recordTeachadvantage, // 教学优点
      recordTeachcontent: records.recordTeachcontent, // 教学内容记录
      recordTeachquestion: records.recordTeachquestion, // 教学问题
      recordListenTimeToString: records.recordListentime // 听课日期
    }
  })
}

export function addsupervisor(instructionalSupervision) {
  return request({
    url: '/instructionalSupervision/insert',
    method: 'post',
    data: {
      instructionalSupervisionEventType: instructionalSupervision.instructionalSupervisionEventType,
      instructionalSupervisionEventTitle: instructionalSupervision.instructionalSupervisionEventTitle,
      instructionalSupervisionEventDescription: instructionalSupervision.instructionalSupervisionEventDescription,
      instructionalSupervisionParticipants: instructionalSupervision.instructionalSupervisionParticipants,
      instructionalSupervisionSolution: instructionalSupervision.instructionalSupervisionSolution,
      instructionalSupervisionExecutionRecord: instructionalSupervision.instructionalSupervisionExecutionRecord,
      instructionalSupervisionEventSummary: instructionalSupervision.instructionalSupervisionEventSummary,
      instructionalSupervisionStatus: instructionalSupervision.instructionalSupervisionStatus
    }
  })
}

export function getTeacherString(id) {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: id
    }
  })
}

export function getAllTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}

export function getClassType() {
  return request({
    url: '/clazzType/one',
    method: 'get'
  })
}

export function getAllClassRoom() {
  return request({
    url: '/room/all',
    method: 'get'
  })
}

export function getAllstudentName() {
  return request({
    url: '/student/selectStudentNameAndAdd',
    method: 'get'
  })
}

export function getAlllesson() {
  return request({
    url: '/lesson/all',
    method: 'get'
  })
}
