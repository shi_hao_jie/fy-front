import request from '@/utils/request'

export function addarea(area) {
  return request({
    url: '/areaType/one/',
    method: 'post',
    params: {
      areaAdvantages: area.areaAdvantages,
      areaDisadvantages: area.areaDisadvantages,
      areaRemarks: area.areaRemarks,

      areaId: area.areaId,
      areaStudentId: area.areaStudentId,
      areaTypeId: area.areaTypeId

    }
  })
}

export function getAllarea() {
  return request({
    url: '/area/all/',
    method: 'get'
  })
}

export function getplanById(id) {
  return request({
    url: '/area/one',
    method: 'get',
    params: {
      areaId: id
    }
  })
}

export function updateplan(plan) {
  return request({
    url: '/plan/',
    method: 'put',
    params: {
      areaAdvantages: area.areaAdvantages,
      areaDisadvantages: area.areaDisadvantages,
      areaRemarks: area.areaRemarks,

      areaId: plan.areaId,
      areaStudentId: plan.areaStudentId,
      areaTypeId: plan.areaTypeId
    }
  })
}

export function deleteplan(id) {
  return request({
    url: '/plan/',
    method: 'delete',
    params: {
      planId: id
    }
  })
}
