import request from '@/utils/request'
import store from '@/store/index'

export function AddSchedule(staff) {
  var a =
{
  schedulePostUserId: store.getters.userId, // 排课人员id
  scheduleTeacherId: staff.scheduleTeacherId, // 授课教师ID
  scheduleClassId: staff.scheduleClassId, // 班级ID
  scheduleClassTime: staff.scheduleClassTime, // 第几节课
  scheduleRoomId: staff.scheduleRoomId, // 房间ID
  scheduleColor: staff.scheduleColor, // 排课显示颜色
  scheduleFontColor: staff.scheduleFontColor, // 字体颜色
  scheduleTimeDay: staff.scheduleTimeDay, // 上课日期
  scheduleCompletionTimes: staff.scheduleCompletionTimes, // 上课次数
  scheduleStatus: 0

}
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/schedule/insert',
    method: 'post',
    data: Data
  })
}

export function getAllSchedule() {
  return request({
    url: '/schedule/all',
    method: 'get',
    params: {
      limit: 1000,
      page: 1
    }
  })
}

export function getScheduleById(scheduleId) {
  return request({
    url: '/schedule/one?id=' + scheduleId,
    method: 'get'

  })
}

export function updateSchedule(staff) {
  return request({
    url: '/schedule/',
    method: 'put',
    params: {
      scheduleId: staff.scheduleId,
      scheduleStaffId: staff.scheduleStaffId,
      teacherId: staff.teacherId,
      classId: staff.classId,
      timeSkcd: staff.timeSkcd,
      timeSkjc: staff.timeSkjc,
      roomId: staff.roomId,
      scheduleColor: staff.scheduleColor,
      fontColor: staff.fontColor,
      timeDaytoString: '2020-10-19',
      weekName: staff.weekName
    }
  })
}

export function deleteSchedule(scheduleId) {
  return request({
    url: '/schedule/delete/?id=' + scheduleId,
    method: 'post'

  })
}

export function getSchedule(staff) {
  return request({
    url: '/schedule/selectByClazzIdAndTime',
    method: 'GET',
    params: {
      clazzId: staff.classId,
      Time: staff.Time
    }
  })
}

export function getScheduleByclassId(stAAA) {
  console.log(stAAA, 9999999)
  return request({
    url: '/schedule/selectByClazzIdAndTime',
    method: 'GET',
    params: {
      clazzId: stAAA.classId,
      Time: stAAA.Time
    }
  })
}

export function getTeacherSchedule(search) {
  return request({
    url: '/schedule/scheduleOfTheDayInDayByTeacher',
    method: 'GET',
    params: {
      day: search.day
    }

  })
}

export function getRoomSchedule(search) {
  return request({
    url: '/schedule/roomscheduleOfTheDayInDay',
    method: 'GET',
    params: {
      day: search.day
    }
  })
}

export function getResourcesSchedule(search) {
  return request({
    url: '/schedule/resourcesScheduleOfTheDayInDay',
    method: 'GET',
    params: {
      day: search.day
    }
  })
}

export function batchAddSchedule(staff) {
  return request({
    url: '/schedule/all/accordingToTheDayOfTheWeek',
    method: 'post',
    data: {
      scheduleStaffId: store.getters.userId,
      teacherId: staff.teacherId,
      classId: staff.classId,
      timeSkcd: staff.timeSkcd,
      timeSkjc: staff.timeSkjc,
      roomId: staff.roomId,
      scheduleColor: staff.scheduleColor,
      fontColor: staff.fontColor,
      timeDay: staff.timeDay,
      scheduleStatus: 0,
      begindate: staff.timeDay,
      times: staff.times,
      scheduleCompletionTimes: staff.times
    }
  })
}
export function getAllRoomName(staff) {
  return request({
    url: '/schedule/checking/room',
    method: 'get',
    params: {
      day: staff.scheduleTimeDay,
      scheduleClassTime: staff.scheduleClassTime
    }

  })
}
export function getAllClass() {
  return request({
    url: '/clazz/all',
    method: 'get',
    params: {
      limit: 1000,
      page: 1
    }
  })
}
export function getStaffStringT(staff) {
  return request({
    url: '/schedule/checking/teacher',
    method: 'get',
    params: {
      day: staff.scheduleTimeDay,
      scheduleClassTime: staff.scheduleClassTime
    }

  })
}
