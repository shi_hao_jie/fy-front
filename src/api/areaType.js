import request from '@/utils/request'

export function addareaType(plan) {
  return request({
    url: '/areaType/one/',
    method: 'post',
    params: {
        areaName: plan.areaName,
        areaTypeId: plan.areaTypeId,

    }
  })
}

export function getAllareaType() {
  return request({
    url: '/areaType/all/',
    method: 'get'
  })
}

export function getplanById(id) {
  return request({
    url: '/area/one',
    method: 'get',
    params: {
      areaTypeId: id
    }
  })
}

export function updateareaType(plan) {
  return request({
    url: '/areaType/',
    method: 'put',
    params: {
        areaName: plan.areaName,
        areaTypeId: plan.areaTypeId,
    }
  })
}

export function deleteareaType(id) {
  return request({
    url: '/areaType/',
    method: 'delete',
    params: {
        areaTypeId: id
    }
  })
}
