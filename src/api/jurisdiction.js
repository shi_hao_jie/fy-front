import request from '@/utils/request'

export function addJurisdiction(jurisdictionId, jurisdictionName) {
  return request({
    url: '/jurisdiction/one',
    method: 'post',
    data: {
      jurisdictionId: jurisdictionId,
      jurisdictionName: jurisdictionName
    }
  })
}

export function getAllJurisdiction() {
  return request({
    url: '/jurisdiction/all',
    method: 'get'
  })
}

export function getJurisdictionById(id) {
  return request({
    url: '/jurisdiction/id',
    method: 'get',
    params: {
      jurisdictionId: id
    }
  })
}

export function updateJurisdiction(jurisdictionId, jurisdictionName) {
  return request({
    url: '/jurisdiction/',
    method: 'put',
    params: {
      jurisdictionId: jurisdictionId,
      jurisdictionName: jurisdictionName
    }
  })
}

export function deleteJurisdiction(id) {
  return request({
    url: '/jurisdiction/id',
    method: 'delete',
    params: {
      jurisdictionId: id
    }
  })
}
