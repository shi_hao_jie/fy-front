import request from '@/utils/request'
import store from '@/store/index'

export function addChild(child) {
  return request({
    url: '/student/one',
    method: 'post',
    data: {
      brothersSituation: child.brothersSituation,
      studentAccompanyTrainer: child.studentAccompanyTrainer,
      studentAddress: child.studentAddress,
      studentBirthtoString: child.studentBirthtoString,
      studentBrothers: child.studentBrothers,
      studentCare: child.studentCare,
      studentFatherId: child.studentFatherId,
      studentId: child.studentId,
      studentJoinCentertoString: child.studentJoinCentertoString,
      studentLanguageEnv: child.studentLanguageEnv,
      studentMotherId: child.studentMotherId,
      studentName: child.studentName,
      studentNation: child.studentNation,
      studentPhone: child.studentPhone,
      studentSex: child.studentSex,
      studentTreatAttention: child.studentTreatAttention,
      studentTreatCurrentDiagnosis: child.studentTreatCurrentDiagnosis,
      studentTreatPastTrainingHistoryId: child.studentTreatPastTrainingHistoryId,
      studentTreatHospital: child.studentTreatHospital,
      studentTreatOtherDiseases: child.studentTreatOtherDiseases,
      studentTreatTrainingAge: child.studentTreatTrainingAge,
      studentTreatTrainingInstitutionRecords: child.studentTreatTrainingInstitutionRecords,
      studentHeadImages: child.studentPic

    }
  })
}

export function addChild2(student) {
  return request({
    url: '/student/parent/one',
    method: 'post',
    data: {
      studentName: student.studentName,
      studentAddress: student.studentAddress
    }
  })
}

export function getAllChild(BB) {
  return request({
    url: '/student/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllChildA(BB) { // 测试接口
  return request({
    url: '/student/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllChildM(BB) { // 测试接口
  return request({
    url: '/student/all/MedicalDirectorIdJurisdictionZero',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}
export function getAllChildByprincipal(BB) { // 测试接口
  return request({
    url: '/student/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllChildB(BB) { // 测试接口
  return request({
    url: '/student/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}
export function getAllChildC() { // 测试接口
  return request({
    url: '/student/all?limit=99999&page=1',
    method: 'get'
  })
}
export function getAllMedicalChild(BB) {
  return request({
    url: '/student/all/MedicalDirectorIdJurisdictionZero', // 根据医疗主管id获取信息
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getChildById(id) {
  return request({
    url: '/student/id',
    method: 'get',
    params: {
      studentId: id
    }
  })
}

export function getAllParent(id) {
  return request({
    url: '/parent/selectParentNameAndCallBySex',
    method: 'get',
    params: {
      sex: id
    }
  })
}

export function updateChild(info) {
  return request({
    url: '/student/newupdate',
    method: 'Post',
    params: {
      studentId: info.studentId,
      studentEducationEvaluation: info.studentEducationEvaluation

    }
  })
}

export function updateMedicalChild(student) {
  var a =
  {
    studentId: student.studentId,
    studentTreatAssessmentInformation: student.studentTreatAssessmentInformation,
    studentTreatTrainingPrescription: student.studentTreatTrainingPrescription,
    studentTreatAttention: student.studentTreatAttention

  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/student/update',
    method: 'post',
    data: Data,
    params: {
      studentId: student.studentId
    }
  })
}

export function deleteChild(id) {
  return request({
    url: '/student/',
    method: 'delete',
    params: {
      parentId: id
    }
  })
}
export function deletetrain(id) {
  return request({
    url: '/trainingInstitutionRecords/delete/',
    method: 'post',
    params: {
      id: id
    }
  })
}
export function addTrain(train) {
  return request({
    url: '/trainingInstitutionRecords/',
    method: 'post',
    data: {
      effect: train.effect,
      orgName: train.orgName,
      studentId: train.studentId,
      staffId: store.getters.userId,
      timeEndtoString: train.timeEndtoString,
      timeStarttoString: train.timeStarttoString
    }
  })
}

export function getTrainById(id) {
  return request({
    url: '/trainingInstitutionRecords/',
    method: 'get',
    params: {
      id: id
    }
  })
}
export function editTrain(train) {
  return request({
    url: '/trainingInstitutionRecords/',
    method: 'put',
    params: {
      effect: train.effect,
      orgName: train.orgName,
      staffId: train.staffId,
      studentId: train.studentId,
      timeEndtoString: train.timeEndtoString,
      timeStarttoString: train.timeStarttoString,
      trainingInstitutionRecordId: train.trainingInstitutionRecordId

    }
  })
}
export function selectStudent() {
  return request({
    url: '/student/selectStudentNameAndAdd',
    method: 'get'
  })
}

export function getAllStudentString() {
  return request({
    url: '/student/selectStudentNameAndAdd',
    method: 'get'
  })
}

export function delTrain(id) {
  return request({
    url: '/trainingInstitutionRecords/',
    method: 'delete',
    params: {
      id: id
    }
  })
}

export function newAdd(student, train) {
  return request({
    url: '/student/new',
    method: 'post',
    data: {
      brothersSituation: student.brothersSituation,
      list: JSON.stringify(train),
      studentAccompanyTrainer: student.studentAccompanyTrainer,
      studentAddress: student.studentAddress,
      studentBirth: student.studentBirth,
      studentBrothers: student.studentBrothers,
      studentCare: student.studentCare,
      studentEducationEvaluation: student.studentEducationEvaluation,
      studentFatherId: student.studentFatherId,
      studentHeadImages: student.studentHeadImages,
      studentJoinCenter: student.studentJoinCenter,
      studentLanguageEnv: student.studentLanguageEnv,
      studentMotherId: student.studentMotherId,
      studentName: student.studentName,
      studentNation: student.studentNation,
      studentPhone: student.studentPhone,
      studentResidence: student.studentResidence,
      studentSex: student.studentSex,
      studentStatus: student.studentStatus,
      studentTreatAssessmentInformation: student.studentTreatAssessmentInformation,
      studentTreatAttention: student.studentTreatAttention,
      studentTreatClinicInformation: student.studentTreatClinicInformation,
      studentTreatCurrentDiagnosis: student.studentTreatCurrentDiagnosis,
      studentTreatDiagnosisCases: student.studentTreatDiagnosisCases,
      studentTreatHospital: student.studentTreatHospital,
      studentTreatOtherDiseases: student.studentTreatOtherDiseases,
      studentTreatPastTrainingHistoryId: student.studentTreatPastTrainingHistoryId,
      studentTreatTime: student.studentTreatTime,
      studentTreatTrainingAge: student.studentTreatTrainingAge,
      studentTreatTrainingInstitutionRecords: student.studentTreatTrainingInstitutionRecords,
      studentTreatTrainingPrescription: student.studentTreatTrainingPrescription,
      studentTreatMedicalDirectorId: student.studentTreatMedicalDirectorId,
      studentTreatPrincipalTeacherId: student.studentTreatPrincipalTeacherId,
      studentTreatTeachingDirectorId: student.studentTreatTeachingDirectorId
    }
  })
}

export function newAddA(student, train) {
  var adminLoginRequestBody = {
    studentName: student.studentName, // 学生姓名
    studentSex: student.studentSex, // 学生性别
    studentBirth: student.studentBirth, // 学生生日
    studentNation: student.studentNation, // 学生民族
    studentLanguageEnv: student.studentLanguageEnv, // 学生语言环境
    studentPhone: student.studentPhone, // 学生联系方式
    studentResidence: student.studentResidence, // 学生户口所在地
    studentAddress: student.studentAddress, // 家庭地址
    studentFatherId: student.studentFatherId, // 父亲id
    studentMotherId: student.studentMotherId, // 母亲id
    studentCare: student.studentCare, // 主要代养人
    studentAccompanyTrainer: student.studentAccompanyTrainer, // 陪同训练人
    studentStatus: student.studentStatus, // 学生状态
    studentPrincipalTeacherId: student.studentPrincipalTeacherId, // 主责老师
    studentTeachingSupervisorId: student.studentTeachingSupervisorId, // 教学主管
    studentMedicalDirectorId: student.studentMedicalDirectorId, // 医疗主管
    studentHeadImages: student.studentHeadImages// 学生头像
  }
  var loginData = JSON.stringify(adminLoginRequestBody)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/student/one',
    method: 'post',
    data: loginData
  })
}

export function newUpdateA(student, train) {
  var adminLoginRequestBody = {
    studentName: student.studentName, // 学生姓名
    studentSex: student.studentSex, // 学生性别
    studentBirth: student.studentBirth, // 学生生日
    studentNation: student.studentNation, // 学生民族
    studentLanguageEnv: student.studentLanguageEnv, // 学生语言环境
    studentPhone: student.studentPhone, // 学生联系方式
    studentResidence: student.studentResidence, // 学生户口所在地
    studentAddress: student.studentAddress, // 家庭地址
    studentFatherId: student.studentFatherId, // 父亲id
    studentMotherId: student.studentMotherId, // 母亲id
    studentCare: student.studentCare, // 主要代养人
    studentAccompanyTrainer: student.studentAccompanyTrainer, // 陪同训练人
    studentStatus: student.studentStatus, // 学生状态
    studentPrincipalTeacherId: student.studentPrincipalTeacherId, // 主责老师
    studentTeachingSupervisorId: student.studentTeachingSupervisorId, // 教学主管
    studentMedicalDirectorId: student.studentMedicalDirectorId, // 医疗主管
    studentHeadImages: student.studentHeadImages// 学生头像
  }
  var loginData = JSON.stringify(adminLoginRequestBody)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/student/update',
    method: 'post',
    params: {
      studentId: student.studentId
    },
    data: loginData
  })
}

export function newUpdate(student, train) {
  var a =
  {

    trainingInstitutionRecordsList: (train),
    studentTreatCurrentDiagnosis: student.studentTreatCurrentDiagnosis,
    studentTreatHospital: student.studentTreatHospital,
    studentTreatTime: student.studentTreatTime,
    studentTreatDiagnosisCases: student.studentTreatDiagnosisCases,
    studentTreatTrainingAge: student.studentTreatTrainingAge

  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/student/update',
    method: 'post',
    data: Data,
    params: {
      studentId: student.studentId
    }
  })
}
