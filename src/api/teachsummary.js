import request from '@/utils/request'
import store from '@/store/index'

export function updateteachsummary(summary) {
  var a =
  {
    teachSummaryEvaluation: summary.teachSummaryEvaluation,
    teachSummaryStudentEvaluation: summary.teachSummaryStudentEvaluation,
    teachSummaryClazzId: summary.teachSummaryClazzId,
    teachSummaryStudentId: summary.teachSummaryStudentId,
    teachSummaryPostUserId: summary.teachSummaryPostUserId,
    teachSummaryId: summary.teachSummaryId // 教学总结id
  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachsummary/update?id=' + summary.teachSummaryId,
    method: 'post',
    data: Data
  })
}

export function updateteachsummaryA(summary, BB) {
  var a =
  {
    teachSummaryEvaluation: summary.teachSummaryEvaluation,
    teachSummaryStudentEvaluation: summary.teachSummaryStudentEvaluation,
    teachSummaryClazzId: summary.teachSummaryClazzId,
    teachSummaryStudentId: summary.teachSummaryStudentId,
    teachSummaryPostUserId: summary.teachSummaryPostUserId,
    teachSummaryId: summary.teachSummaryId, // 教学总结id
    teachSummaryReplyPrincipalTeacherContent: summary.teachSummaryReplyPrincipalTeacherContent
  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachsummary/update?id=' + summary.teachSummaryId,
    method: 'post',
    data: Data
  })
}

export function updateteachsummaryB(summary, BB) {
  var a =
  {
    teachSummaryEvaluation: summary.teachSummaryEvaluation,
    teachSummaryStudentEvaluation: summary.teachSummaryStudentEvaluation,
    teachSummaryClazzId: summary.teachSummaryClazzId,
    teachSummaryStudentId: summary.teachSummaryStudentId,
    teachSummaryPostUserId: summary.teachSummaryPostUserId,
    teachSummaryId: summary.teachSummaryId, // 教学总结id
    teachSummaryReplyTeachingDirectorContent: summary.teachSummaryReplyTeachingDirectorContent
  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachsummary/update?id=' + summary.teachSummaryId,
    method: 'post',
    data: Data
  })
}
export function deleteteachsummary(id) {
  return request({
    url: '/teachsummary/delete/',
    method: 'POST',
    params: {
      teachSummaryId: id
    }
  })
}

export function getAllteachsummary(BB) {
  return request({
    url: '/teachsummary/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryByMedical(BB) {
  return request({
    url: '/teachsummary/all/MedicalDirectorIdJurisdictionZero',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryByDirector(BB) {
  return request({
    url: '/teachsummary/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryByprincipal(BB) {
  return request({
    url: '/teachsummary/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachsummaryAA() {
  return request({
    url: '/teachsummary/all',
    method: 'get'

  })
}

export function upateStatus(summary) {
  var a =
  {
    teachSummaryId: summary.teachSummaryId,
    teachSummaryStatus: summary.teachSummaryStatus = 3
  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachsummary/update/status',
    method: 'post',
    data: Data
  })
}
export function upateStatusA(aaa) {
  var a =
  {

    teachSummaryReplyPrincipalTeacherId: store.getters.userId, // 教学总结id
    teachSummaryStatus: 1
  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachsummary/update/replyPrincipalTeacher',
    method: 'post',
    data: Data,
    params: {
      id: aaa.teachSummaryId
    }
  })
}

export function upateStatusB(aaa) {
  var a =
  {
    teachSummaryReplyTeachingDirectorId: store.getters.userId,
    teachSummaryStatus: 2
  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachsummary/update/replyTeachingDirector',
    method: 'post',
    data: Data,
    params: {
      id: aaa.teachSummaryId
    }
  })
}
export function selectclazz() {
  return request({
    url: '/clazz/all',
    method: 'get',
    params: {
      limit: 100,
      page: 1
    }

  })
}

export function getteachsummarybyId(id) {
  return request({
    url: '/teachsummary/select',
    method: 'get',
    params: {
      teachSummaryId: id
    }
  })
}

export function addoneteachsummary(summary) {
  var a =
  {
    teachSummaryEvaluation: summary.teachSummaryEvaluation,
    teachSummaryStudentEvaluation: summary.teachSummaryStudentEvaluation,
    teachSummaryClazzId: summary.teachSummaryClazzId,
    teachSummaryStudentId: summary.teachSummaryStudentId,
    teachSummaryPostUserId: summary.teachSummaryPostUserId,
    teachSummaryStatus: 0
  }
  var Data = JSON.stringify(a)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachsummary/insert',
    method: 'post',
    data: Data
  })
}

// export function getAllstudentName() {
//   return request({
//     url: "/student/selectStudentNameAndAdd",
//     method: "get"
//   });
// }
