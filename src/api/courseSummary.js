import request from '@/utils/request'
import store from '@/store/index'

export function addcourseSummer(courseSummary3, BB) {
  var courseSummary =
  {
    // courseSummaryStudentId: courseSummary1.courseSummaryStudentId,
    // courseSummaryClazzId: courseSummary1.courseSummaryClazzId,
    // courseSummaryTeacherId: courseSummary1.courseSummaryTeacherId,
    // courseSummaryTeachingSupervisorId: courseSummary1.courseSummaryTeachingSupervisorId,
    // courseSummaryMedicalDirectorId: courseSummary1.courseSummaryMedicalDirectorId,
    // courseSummaryImplementation: courseSummary1.courseSummaryImplementation, // 执行情况
    // courseSummaryCourseEvaluation: courseSummary1.courseSummaryCourseEvaluation, // 课程评价
    // courseSummaryStudentEvaluation: courseSummary1.courseSummaryStudentEvaluation, // 学生评价
    // courseSummaryPostUserId: store.getters.userId,
    // courseSummaryStatus: 0,
    // courseSummaryClazzName: courseSummary1.courseSummaryClazzName,
    // courseSummaryId: courseSummary1.courseSummaryId,
    // courseSummaryMedicalDirectorName: courseSummary1.courseSummaryMedicalDirectorName,
    // courseSummaryPostTime: courseSummary1.courseSummaryPostTime,
    // courseSummaryPostUserName: courseSummary1.courseSummaryPostUserName,
    // courseSummaryReplyPrincipalTeacherContent: courseSummary1.courseSummaryReplyPrincipalTeacherContent,
    // courseSummaryReplyPrincipalTeacherId: courseSummary1.courseSummaryReplyPrincipalTeacherId,
    // courseSummaryReplyPrincipalTeacherName: courseSummary1.courseSummaryReplyPrincipalTeacherName,
    // courseSummaryReplyPrincipalTeacherTime: courseSummary1.courseSummaryReplyPrincipalTeacherTime,
    // courseSummaryReplyTeachingDirectorContent: courseSummary1.courseSummaryReplyTeachingDirectorContent,
    // courseSummaryReplyTeachingDirectorId: courseSummary1.courseSummaryReplyTeachingDirectorId,
    // courseSummaryReplyTeachingDirectorName: courseSummary1.courseSummaryReplyTeachingDirectorName,
    // courseSummaryReplyTeachingDirectorTime: courseSummary1.courseSummaryReplyTeachingDirectorTime,
    // courseSummaryStudentName: courseSummary1.courseSummaryStudentName,
    // courseSummaryTeacherName: courseSummary1.courseSummaryTeacherName,
    // courseSummaryTeachingSupervisorName: courseSummary1.courseSummaryTeachingSupervisorName,
    // ourseSummaryClazzTypeName: courseSummary1.courseSummaryClazzTypeName,
    // courseSummaryTechnicalTypeName: courseSummary1.courseSummaryTechnicalTypeName

    courseSummaryClazzId: courseSummary3.courseSummaryClazzId,
    courseSummaryClazzName: '',
    courseSummaryCourseEvaluation: courseSummary3.courseSummaryCourseEvaluation,
    courseSummaryId: courseSummary3.courseSummaryId,
    courseSummaryImplementation: courseSummary3.courseSummaryImplementation,
    courseSummaryMedicalDirectorId: courseSummary3.courseSummaryMedicalDirectorId,
    courseSummaryMedicalDirectorName: '',
    courseSummaryPostTime: '',
    courseSummaryPostUserId: BB,
    courseSummaryPostUserName: '',
    courseSummaryReplyPrincipalTeacherContent: courseSummary3.courseSummaryReplyPrincipalTeacherContent,
    courseSummaryReplyPrincipalTeacherId: courseSummary3.courseSummaryReplyPrincipalTeacherId,
    courseSummaryReplyPrincipalTeacherName: '',
    courseSummaryReplyPrincipalTeacherTime: '',
    courseSummaryReplyTeachingDirectorContent: courseSummary3.courseSummaryReplyTeachingDirectorContent,
    courseSummaryReplyTeachingDirectorId: courseSummary3.courseSummaryReplyTeachingDirectorId,
    courseSummaryReplyTeachingDirectorName: '',
    courseSummaryReplyTeachingDirectorTime: '',
    courseSummaryStatus: courseSummary3.courseSummaryStatus,
    courseSummaryStudentEvaluation: courseSummary3.courseSummaryStudentEvaluation,
    courseSummaryStudentId: courseSummary3.courseSummaryStudentId,
    courseSummaryStudentName: '',
    courseSummaryTeacherId: courseSummary3.courseSummaryTeacherId,
    courseSummaryTeacherName: '',
    courseSummaryTeachingSupervisorId: courseSummary3.courseSummaryTeachingSupervisorId,
    courseSummaryTeachingSupervisorName: ''
  }
  var addcourseSummerData = JSON.stringify(courseSummary)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/courseSummary/insert',
    method: 'post',
    data: addcourseSummerData
  })
}

export function getAll() { // 获取所有课程总结
  return request({
    url: '/courseSummary/all',
    method: 'get'
  })
}

export function getAllcourseSummary(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB,
      limit: 999,
      page: 1
    }
  })
}

export function getAllcourseSummaryByDirector(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB,
      limit: 999,
      page: 1

    }
  })
}
export function getAllcourseSummaryByMedical(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/MedicalDirectorIdJurisdictionZero',
    method: 'get',
    params: {

      staffId: BB,
      limit: 100,
      page: 1
    }
  })
}

export function getAllcourseSummaryByprincipal(BB) { // 获取所有课程总结
  return request({
    url: '/courseSummary/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllcourseSummaryAAA() { // 获取所有课程总结
  return request({
    url: '/courseSummary/all',
    method: 'get'

  })
}

export function getcourseSummarybyId(id) {
  return request({
    url: 'courseSummary/one',
    method: 'get',
    params: {
      id: id // 根据计划id获取教学总结
    }
  })
}

export function updatecourseSummaryId(courseSummary2) {
  var courseSummary =
  {
    // ourseSummaryClazzId: courseSummary2.courseSummaryClazzId,
    // courseSummaryClazzName: courseSummary2.courseSummaryClazzName,
    // courseSummaryCourseEvaluation: courseSummary2.courseSummaryCourseEvaluation, // 课程评价,
    // courseSummaryId: courseSummary2.courseSummaryId, // 课程总结id,
    // courseSummaryImplementation: courseSummary2.courseSummaryImplementation, // 执行情况,
    // courseSummaryMedicalDirectorId: courseSummary2.courseSummaryMedicalDirectorId,
    // courseSummaryMedicalDirectorName: courseSummary2.courseSummaryMedicalDirectorName,
    // courseSummaryPostTime: courseSummary2.courseSummaryPostTime,
    // courseSummaryPostUserId: courseSummary2.courseSummaryPostUserId,
    // courseSummaryPostUserName: courseSummary2.courseSummaryPostUserName,
    // courseSummaryReplyPrincipalTeacherContent: courseSummary2.courseSummaryReplyPrincipalTeacherContent,
    // courseSummaryReplyPrincipalTeacherId: courseSummary2.courseSummaryReplyPrincipalTeacherId,
    // courseSummaryReplyPrincipalTeacherName: courseSummary2.courseSummaryReplyPrincipalTeacherName,
    // courseSummaryReplyPrincipalTeacherTime: courseSummary2.courseSummaryReplyPrincipalTeacherTime,
    // courseSummaryReplyTeachingDirectorContent: courseSummary2.courseSummaryReplyTeachingDirectorContent,
    // courseSummaryReplyTeachingDirectorId: courseSummary2.courseSummaryReplyTeachingDirectorId,
    // courseSummaryReplyTeachingDirectorName: courseSummary2.courseSummaryReplyTeachingDirectorName,
    // courseSummaryReplyTeachingDirectorTime: courseSummary2.courseSummaryReplyTeachingDirectorTime,
    // courseSummaryStatus: courseSummary2.courseSummaryStatus,
    // courseSummaryStudentEvaluation: courseSummary2.courseSummaryStudentEvaluation, // 学生评价,
    // courseSummaryStudentId: courseSummary2.courseSummaryStudentId,
    // courseSummaryStudentName: courseSummary2.courseSummaryStudentName,
    // courseSummaryTeacherId: courseSummary2.courseSummaryTeacherId,
    // courseSummaryTeacherName: courseSummary2.courseSummaryTeacherName,
    // courseSummaryTeachingSupervisorId: courseSummary2.courseSummaryTeachingSupervisorId,
    // courseSummaryTeachingSupervisorName: courseSummary2.courseSummaryTeachingSupervisorName,
    // ourseSummaryClazzTypeName: courseSummary2.courseSummaryClazzTypeName,
    // courseSummaryTechnicalTypeName: courseSummary2.courseSummaryTechnicalTypeName

    courseSummaryClazzId: courseSummary2.courseSummaryClazzId,
    courseSummaryClazzName: '',
    courseSummaryClazzTypeName: '',
    courseSummaryCourseEvaluation: courseSummary2.courseSummaryCourseEvaluation,
    courseSummaryId: courseSummary2.courseSummaryId,
    courseSummaryImplementation: courseSummary2.courseSummaryImplementation,
    courseSummaryMedicalDirectorId: courseSummary2.courseSummaryMedicalDirectorId,
    courseSummaryMedicalDirectorName: '',
    courseSummaryPostTime: '',
    courseSummaryPostUserId: courseSummary2.courseSummaryPostUserId,
    courseSummaryPostUserName: '',
    courseSummaryReplyPrincipalTeacherContent: courseSummary2.courseSummaryReplyPrincipalTeacherContent,
    courseSummaryReplyPrincipalTeacherId: courseSummary2.courseSummaryReplyPrincipalTeacherId,
    courseSummaryReplyPrincipalTeacherName: '',
    courseSummaryReplyPrincipalTeacherTime: '',
    courseSummaryReplyTeachingDirectorContent: courseSummary2.courseSummaryReplyTeachingDirectorContent,
    courseSummaryReplyTeachingDirectorId: courseSummary2.courseSummaryReplyTeachingDirectorId,
    courseSummaryReplyTeachingDirectorName: '',
    courseSummaryReplyTeachingDirectorTime: '',
    courseSummaryStatus: courseSummary2.courseSummaryStatus,
    courseSummaryStudentEvaluation: courseSummary2.courseSummaryStudentEvaluation,
    courseSummaryStudentId: courseSummary2.courseSummaryStudentId,
    courseSummaryStudentName: '',
    courseSummaryTeacherId: courseSummary2.courseSummaryTeacherId,
    courseSummaryTeacherName: '',
    courseSummaryTeachingSupervisorId: courseSummary2.courseSummaryTeachingSupervisorId,
    courseSummaryTeachingSupervisorName: '',
    courseSummaryTechnicalTypeName: ''

    // courseSummaryId: courseSummary2.courseSummaryId, // 课程总结id
    // courseSummaryImplementation: courseSummary2.courseSummaryImplementation, // 执行情况
    // courseSummaryCourseEvaluation: courseSummary2.courseSummaryCourseEvaluation, // 课程评价
    // courseSummaryStudentEvaluation: courseSummary2.courseSummaryStudentEvaluation, // 学生评价
    // courseSummaryStudentId: courseSummary2.courseSummaryStudentId,
    // courseSummaryClazzId: courseSummary2.courseSummaryClazzId,
    // courseSummaryTeacherId: courseSummary2.courseSummaryTeacherId,
    // courseSummaryTeachingSupervisorId: courseSummary2.courseSummaryTeachingSupervisorId,
    // courseSummaryMedicalDirectorId: courseSummary2.courseSummaryMedicalDirectorId
  }
  var updatecourseSummaryIdData = JSON.stringify(courseSummary)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/courseSummary/update',
    method: 'post',
    data: updatecourseSummaryIdData,
    params: {
      courseSummaryId: courseSummary2.courseSummaryId
    }
  })
}

export function deleteplan(id) {
  return request({
    url: '/plan/',
    method: 'delete',
    params: {
      planId: id
    }
  })
}

export function replyPrincipalTeacher(courseSummary3) { // 主责老师批复
  var
    courseSummary = {
      courseSummaryClazzId: courseSummary3.courseSummaryClazzId,
      courseSummaryClazzName: '',
      courseSummaryCourseEvaluation: courseSummary3.courseSummaryCourseEvaluation,
      courseSummaryId: courseSummary3.courseSummaryId,
      courseSummaryImplementation: courseSummary3.courseSummaryImplementation,
      courseSummaryMedicalDirectorId: courseSummary3.courseSummaryMedicalDirectorId,
      courseSummaryMedicalDirectorName: '',
      courseSummaryPostTime: '',
      courseSummaryPostUserId: courseSummary3.courseSummaryPostUserId,
      courseSummaryPostUserName: '',
      courseSummaryReplyPrincipalTeacherContent: courseSummary3.courseSummaryReplyPrincipalTeacherContent,
      courseSummaryReplyPrincipalTeacherId: courseSummary3.courseSummaryReplyPrincipalTeacherId,
      courseSummaryReplyPrincipalTeacherName: '',
      courseSummaryReplyPrincipalTeacherTime: '',
      courseSummaryReplyTeachingDirectorContent: courseSummary3.courseSummaryReplyTeachingDirectorContent,
      courseSummaryReplyTeachingDirectorId: courseSummary3.courseSummaryReplyTeachingDirectorId,
      courseSummaryReplyTeachingDirectorName: '',
      courseSummaryReplyTeachingDirectorTime: '',
      courseSummaryStatus: (courseSummary3.courseSummaryStatus === 0 && courseSummary3.courseSummaryReplyPrincipalTeacherContent.length >= 0) ? 1 : courseSummary3.courseSummaryStatus,
      courseSummaryStudentEvaluation: courseSummary3.courseSummaryStudentEvaluation,
      courseSummaryStudentId: courseSummary3.courseSummaryStudentId,
      courseSummaryStudentName: '',
      courseSummaryTeacherId: courseSummary3.courseSummaryTeacherId,
      courseSummaryTeacherName: '',
      courseSummaryTeachingSupervisorId: courseSummary3.courseSummaryTeachingSupervisorId,
      courseSummaryTeachingSupervisorName: ''
    }
  var replyPrincipalTeacherData = JSON.stringify(courseSummary)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/courseSummary/update/replyPrincipalTeacher',
    method: 'post',
    data: replyPrincipalTeacherData,
    params: {
      courseSummaryId: courseSummary3.courseSummaryId
    }
  })
}

export function replyTeachingDirector(courseSummary_) { // 教学主管批复
  var courseSummary = {
    courseSummaryClazzId: courseSummary_.courseSummaryClazzId,
    courseSummaryClazzName: courseSummary_.courseSummaryClazzName,
    courseSummaryCourseEvaluation: courseSummary_.courseSummaryCourseEvaluation,
    courseSummaryId: courseSummary_.courseSummaryId, // 课程总结id,
    courseSummaryImplementation: courseSummary_.courseSummaryImplementation,
    courseSummaryMedicalDirectorId: courseSummary_.courseSummaryMedicalDirectorId,
    courseSummaryMedicalDirectorName: courseSummary_.courseSummaryMedicalDirectorName,
    courseSummaryPostTime: courseSummary_.courseSummaryPostTime,
    courseSummaryPostUserId: courseSummary_.courseSummaryPostUserId,
    courseSummaryPostUserName: courseSummary_.courseSummaryPostUserName,
    courseSummaryReplyPrincipalTeacherContent: courseSummary_.courseSummaryReplyPrincipalTeacherContent,
    courseSummaryReplyPrincipalTeacherId: courseSummary_.courseSummaryReplyPrincipalTeacherId,
    courseSummaryReplyPrincipalTeacherName: courseSummary_.courseSummaryReplyPrincipalTeacherName,
    courseSummaryReplyPrincipalTeacherTime: courseSummary_.courseSummaryReplyPrincipalTeacherTime,
    courseSummaryReplyTeachingDirectorContent: courseSummary_.courseSummaryReplyTeachingDirectorContent,
    courseSummaryReplyTeachingDirectorId: store.getters.userId,
    courseSummaryReplyTeachingDirectorName: courseSummary_.courseSummaryReplyTeachingDirectorName,
    courseSummaryReplyTeachingDirectorTime: courseSummary_.courseSummaryReplyTeachingDirectorTime,
    courseSummaryStatus: (courseSummary_.courseSummaryStatus === 1 && courseSummary_.courseSummaryReplyPrincipalTeacherContent.length >= 0) ? 2 : courseSummary_.courseSummaryStatus,
    courseSummaryStudentEvaluation: courseSummary_.courseSummaryStudentEvaluation,
    courseSummaryStudentId: courseSummary_.courseSummaryStudentId,
    courseSummaryStudentName: courseSummary_.courseSummaryStudentName,
    courseSummaryTeacherId: courseSummary_.courseSummaryTeacherId,
    courseSummaryTeacherName: courseSummary_.courseSummaryTeacherName,
    courseSummaryTeachingSupervisorId: courseSummary_.courseSummaryTeachingSupervisorId,
    courseSummaryTeachingSupervisorName: courseSummary_.courseSummaryTeachingSupervisorName,
    ourseSummaryClazzTypeName: courseSummary_.courseSummaryClazzTypeName,
    courseSummaryTechnicalTypeName: courseSummary_.courseSummaryTechnicalTypeName

    // courseSummaryClazzId: courseSummary3.courseSummaryClazzId,
    // courseSummaryClazzName: '',
    // courseSummaryCourseEvaluation: courseSummary3.courseSummaryCourseEvaluation,
    // courseSummaryId: courseSummary3.courseSummaryId,
    // courseSummaryImplementation: courseSummary3.courseSummaryImplementation,
    // courseSummaryMedicalDirectorId: courseSummary3.courseSummaryMedicalDirectorId,
    // courseSummaryMedicalDirectorName: '',
    // courseSummaryPostTime: '',
    // courseSummaryPostUserId: courseSummary3.courseSummaryPostUserId,
    // courseSummaryPostUserName: '',
    // courseSummaryReplyPrincipalTeacherContent: courseSummary3.courseSummaryReplyPrincipalTeacherContent,
    // courseSummaryReplyPrincipalTeacherId: courseSummary3.courseSummaryReplyPrincipalTeacherId,
    // courseSummaryReplyPrincipalTeacherName: '',
    // courseSummaryReplyPrincipalTeacherTime: '',
    // courseSummaryReplyTeachingDirectorContent: courseSummary3.courseSummaryReplyTeachingDirectorContent,
    // courseSummaryReplyTeachingDirectorId: store.getters.userId,,
    // courseSummaryReplyTeachingDirectorName: '',
    // courseSummaryReplyTeachingDirectorTime: '',
    // courseSummaryStatus: courseSummary3.courseSummaryStatus,
    // courseSummaryStudentEvaluation: courseSummary3.courseSummaryStudentEvaluation,
    // courseSummaryStudentId: courseSummary3.courseSummaryStudentId,
    // courseSummaryStudentName: '',
    // courseSummaryTeacherId: courseSummary3.courseSummaryTeacherId,
    // courseSummaryTeacherName: '',
    // courseSummaryTeachingSupervisorId: courseSummary3.courseSummaryTeachingSupervisorId,
    // courseSummaryTeachingSupervisorName: ''

    // courseSummaryId: courseSummary_.courseSummaryId, // 课程总结id
    // courseSummaryReplyTeachingDirectorContent: courseSummary_.courseSummaryReplyTeachingDirectorContent,
    // courseSummaryReplyTeachingDirectorId: store.getters.userId,
    // courseSummaryStatus: 2
  }
  var replyTeachingDirectorData = JSON.stringify(courseSummary)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/courseSummary/update/replyTeachingDirector',
    method: 'post',
    data: replyTeachingDirectorData,
    params: {
      courseSummaryId: courseSummary_.courseSummaryId
    }
  })
}

export function updateStatus(courseSummary2) {
  var courseSummary =
  {
    // ourseSummaryClazzId: courseSummary.courseSummaryClazzId,
    // courseSummaryClazzName: courseSummary.courseSummaryClazzName,
    // courseSummaryCourseEvaluation: courseSummary.courseSummaryCourseEvaluation, // 课程评价,
    // courseSummaryId: courseSummary.courseSummaryId, // 课程总结id,
    // courseSummaryImplementation: courseSummary.courseSummaryImplementation, // 执行情况,
    // courseSummaryMedicalDirectorId: courseSummary.courseSummaryMedicalDirectorId,
    // courseSummaryMedicalDirectorName: courseSummary.courseSummaryMedicalDirectorName,
    // courseSummaryPostTime: courseSummary.courseSummaryPostTime,
    // courseSummaryPostUserId: courseSummary.courseSummaryPostUserId,
    // courseSummaryPostUserName: courseSummary.courseSummaryPostUserName,
    // courseSummaryReplyPrincipalTeacherContent: courseSummary.courseSummaryReplyPrincipalTeacherContent,
    // courseSummaryReplyPrincipalTeacherId: courseSummary.courseSummaryReplyPrincipalTeacherId,
    // courseSummaryReplyPrincipalTeacherName: courseSummary.courseSummaryReplyPrincipalTeacherName,
    // courseSummaryReplyPrincipalTeacherTime: courseSummary.courseSummaryReplyPrincipalTeacherTime,
    // courseSummaryReplyTeachingDirectorContent: courseSummary.courseSummaryReplyTeachingDirectorContent,
    // courseSummaryReplyTeachingDirectorId: courseSummary.courseSummaryReplyTeachingDirectorId,
    // courseSummaryReplyTeachingDirectorName: courseSummary.courseSummaryReplyTeachingDirectorName,
    // courseSummaryReplyTeachingDirectorTime: courseSummary.courseSummaryReplyTeachingDirectorTime,
    // courseSummaryStatus: 3,
    // courseSummaryStudentEvaluation: courseSummary.courseSummaryStudentEvaluation, // 学生评价,
    // courseSummaryStudentId: courseSummary.courseSummaryStudentId,
    // courseSummaryStudentName: courseSummary.courseSummaryStudentName,
    // courseSummaryTeacherId: courseSummary.courseSummaryTeacherId,
    // courseSummaryTeacherName: courseSummary.courseSummaryTeacherName,
    // courseSummaryTeachingSupervisorId: courseSummary.courseSummaryTeachingSupervisorId,
    // courseSummaryTeachingSupervisorName: courseSummary.courseSummaryTeachingSupervisorName,
    // ourseSummaryClazzTypeName: courseSummary.courseSummaryClazzTypeName,
    // courseSummaryTechnicalTypeName: courseSummary.courseSummaryTechnicalTypeName

    // courseSummaryId: courseSummary5.courseSummaryId, // 课程总结id
    // courseSummaryStatus: 3

    courseSummaryClazzId: courseSummary2.courseSummaryClazzId,
    courseSummaryClazzName: '',
    courseSummaryClazzTypeName: '',
    courseSummaryCourseEvaluation: courseSummary2.courseSummaryCourseEvaluation,
    courseSummaryId: courseSummary2.courseSummaryId,
    courseSummaryImplementation: courseSummary2.courseSummaryImplementation,
    courseSummaryMedicalDirectorId: courseSummary2.courseSummaryMedicalDirectorId,
    courseSummaryMedicalDirectorName: '',
    courseSummaryPostTime: '',
    courseSummaryPostUserId: courseSummary2.courseSummaryPostUserId,
    courseSummaryPostUserName: '',
    courseSummaryReplyPrincipalTeacherContent: courseSummary2.courseSummaryReplyPrincipalTeacherContent,
    courseSummaryReplyPrincipalTeacherId: courseSummary2.courseSummaryReplyPrincipalTeacherId,
    courseSummaryReplyPrincipalTeacherName: '',
    courseSummaryReplyPrincipalTeacherTime: '',
    courseSummaryReplyTeachingDirectorContent: courseSummary2.courseSummaryReplyTeachingDirectorContent,
    courseSummaryReplyTeachingDirectorId: courseSummary2.courseSummaryReplyTeachingDirectorId,
    courseSummaryReplyTeachingDirectorName: '',
    courseSummaryReplyTeachingDirectorTime: '',
    courseSummaryStatus: 3,
    courseSummaryStudentEvaluation: courseSummary2.courseSummaryStudentEvaluation,
    courseSummaryStudentId: courseSummary2.courseSummaryStudentId,
    courseSummaryStudentName: '',
    courseSummaryTeacherId: courseSummary2.courseSummaryTeacherId,
    courseSummaryTeacherName: '',
    courseSummaryTeachingSupervisorId: courseSummary2.courseSummaryTeachingSupervisorId,
    courseSummaryTeachingSupervisorName: '',
    courseSummaryTechnicalTypeName: ''

  }
  var updateStatusData = JSON.stringify(courseSummary)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/courseSummary/update',
    method: 'post',
    data: updateStatusData,
    params: {
      courseSummaryId: courseSummary2.courseSummaryId
    }
  })
}
