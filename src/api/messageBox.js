import request from '@/utils/request'
import store from '@/store/index'

export function getAllMessage() {
  return request({
    url: '/messagebox/all',
    method: 'get'
  })
}

export function getMessageById(messageboxId) {
  return request({
    url: '/messagebox/id',
    method: 'get',
    params: {
      messageboxId: messageboxId
    }
  })
}

export function getMessageByUser() {
  return request({
    url: '/messagebox/receiveUserId',
    method: 'get',
    params: {
      messageBoxReceiveUserId: store.getters.userId
    }
  })
}

export function updateMessageStatus(messageboxId, status) {
  // return request({
  //   url: '/messagebox/status',
  //   method: 'post',
  //   params: {
  //     messageboxId: messageboxId,
  //     status: status
  //   }
  // })
  var messageboxUpdateStatusRequestBody =
  {
    messageboxId: parseInt(messageboxId),
    updatedMessageboxStatus: parseInt(status)
  }
  var Boxdata = JSON.stringify(messageboxUpdateStatusRequestBody)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/messagebox/update/status',
    method: 'post',
    data: Boxdata
  })
}
