import request from '@/utils/request'

export function addRoom(room) {
  return request({
    url: '/room/',
    method: 'post',
    data: {
      roomInfo: room.roomInfo,
      roomLoc: room.roomLoc,
      roomStatus: room.roomStatus

    }
  })
}

export function getAllRoom() {
  return request({
    url: '/room/all',
    method: 'get'
  })
}

export function getRoomById(id) {
  return request({
    url: '/room/',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function delRoom(id) {
  return request({
    url: '/room/',
    method: 'delete',
    params: {
      id: id
    }
  })
}

export function editRoom(room) {
  return request({
    url: '/room/',
    method: 'put',
    params: {
      roomId: room.roomId,
      roomInfo: room.roomInfo,
      roomLoc: room.roomLoc,
      roomStatus: room.roomStatus
    }
  })
}

export function getAllRoomName() {
  return request({
    url: '/room/all',
    method: 'get'

  })
}
