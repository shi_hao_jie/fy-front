import request from '@/utils/request'

export function addFather(father) {
  var adminLoginRequestBody =
  {
    parentName: father.parentName,
    parentBirth: father.parentBirth,
    parentCall: father.parentCall,
    parentEmail: father.parentEmail,
    parentAdd: father.parentAdd,
    parentEducationalLevel: father.parentEducationalLevel,
    parentOccupation: father.parentOccupation,
    parentSex: 1,
    parentStatus: 0,
    parentIdcard: father.parentIdcard
  }
  var loginData = JSON.stringify(adminLoginRequestBody)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/parent/one',
    method: 'post',
    data: loginData
  })
}
export function addMother(mother) {
  var adminLoginRequestBody =
  {
    parentName: mother.parentName,
    parentBirth: mother.parentBirth,
    parentCall: mother.parentCall,
    parentEmail: mother.parentEmail,
    parentAdd: mother.parentAdd,
    parentEducationalLevel: mother.parentEducationalLevel,
    parentOccupation: mother.parentOccupation,
    parentSex: 0,
    parentStatus: 0,
    parentIdcard: mother.parentIdcard
  }
  var loginData = JSON.stringify(adminLoginRequestBody)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/parent/one',
    method: 'post',
    data: loginData
  })
}

export function addParent(parent) {
  return request({
    url: '/parent/one',
    method: 'post',
    data: {
      parentAdd: parent.parentAdd,
      parentBirthtoString: parent.parentBirthtoString,
      parentCall: parent.parentCall,
      parentEducationalLevel: parent.parentEducationalLevel,
      parentEmail: parent.parentEmail,
      parentName: parent.parentName,
      parentOccupation: parent.parentOccupation,
      parentSex: parent.parentSex,
      parentStatus: 0
    }
  })
}

export function getAllParent(id) {
  return request({
    url: '/parent/all',
    method: 'get'
  })
}

export function getParentById(id) {
  return request({
    url: '/parent/id',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function getParentString() {
  return request({
    url: '/parent/selectParentNameAndCall',
    method: 'get'
  })
}

export function updateParent(parent) {
  return request({
    url: '/staff/',
    method: 'put',
    params: {
      parentId: parent.parentId,
      parentAdd: parent.parentAdd,
      parentBirthtoString: parent.parentBirthtoString,
      parentCall: parent.parentCall,
      parentEducationalLevel: parent.parentEducationalLevel,
      parentEmail: parent.parentEmail,
      parentName: parent.parentName,
      parentOccupation: parent.parentOccupation,
      parentSex: parent.parentSex,
      parentStatus: parent.parentStatus
    }
  })
}

export function deleteParent(id) {
  return request({
    url: '/parent/',
    method: 'delete',
    params: {
      parentId: id
    }
  })
}
