import request from "@/utils/request";
import store from '@/store/index'
export function updateStudentComm(studentcomm) {
  return request({
    url: "/comment/",
    method: "put",
    params: {
        commId:studentcomm.commId,//	评论id
        commByReviewers: studentcomm.commByReviewers, //被评论人id	
        commClassId: studentcomm.commClassId, //班级id
        commClassSkcd: studentcomm.commClassSkcd, //上课次数
        commInfo: studentcomm.commInfo, //评论内容	
        commReviewers: studentcomm.commReviewers, //评论人id
        commType: '3;1', //评论类型(学生1,家长2,老师3,班级4) 例: 存储关系(1;2 学生;家长) 评论人在前被评论人在后面
        commUpdatetimeToString: studentcomm.commUpdatetime, //评论修改时间
    }
  });
}

export function deleteStudentcomm(id) {
  return request({
    url: "/comment/",
    method: "delete",
    params: {
        commentId: id
    }
  });
}
//管理员获取
export function getAllstudentcomm1() {
    return request({
      url: "/comment/admin/all",
      method: "get"
    });
  }
//根据员工或老师id获取
export function getAllstudentcomm() {
  return request({
    url: "/comment/admin/select",
    method: "get",
    params: {
        studentcommId: store.getters.userId
    }
  });
}
//根据评论id查找评论(管理员使用)
export function getStudentcommbyId1(id) {
    return request({
      url: "/comment/admin/select",
      method: "get",
      params: {
        commentId: id
      }
    });
  }

//根据评论id查找评论
export function getStudentcommbyId(id) {
    return request({
      url: "/comment/admin/select",
      method: "get",
      params: {
        commentId: id,
        userId: store.getters.userId
      }
    });
  }

export function addonestudentcomm(studentcomm) {
  return request({
    url: "/comment/insert",
    method: "post",
    data: {
        commByReviewers: studentcomm.commByReviewers, //被评论人id	
        commClassId: studentcomm.commClassId, //班级id
        commClassSkcd: studentcomm.commClassSkcd, //第几次课
        commCreattimeToString: studentcomm.commUpdatetime, //评论创建时间
        commUpdatetimeToString: studentcomm.commUpdatetime, //评论修改时间
        commInfo: studentcomm.commInfo, //评论内容	
        commReviewers: store.getters.userId, //评论人id
        commType: "3;1", //评论类型(学生1,家长2,老师3,班级4) 例: 存储关系(1;2 学生;家长) 评论人在前被评论人在后面
        PStaffidDirector: studentcomm.pstaffidDirector, //教学主管id
    }
  });
}
// export function getTeacherString(id) {
//   return request({
//     url: "/staff/selectStaffnameAndStaffId/ByJurisdiction",
//     method: "get",
//     params: {
//         Jurisdiction: id
//     }
//   });
// }
  

// 获取学生信息//
export function getStudentString(id) {
  return request({
    url: '/student/selectStudentNameAndAddByClassId',
    method: 'get',
    params: {
      ClassId: id
    }
  })
}
