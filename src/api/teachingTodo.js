import request from '@/utils/request'

export function addemergency(emergency) {
  return request({
    url: '/emergency/one',
    method: 'post',
    data: {
      emergencyClazzId: emergency.emergencyClazzId,
      emergencyClazzSkcd: emergency.emergencyClazzSkcd,
      emergencyConclusion: emergency.emergencyConclusion,
      emergencySolution: emergency.emergencySolution,
      emergencyExecute: emergency.emergencyExecute,
      emergencyStudentId: emergency.emergencyStudentId,
      emergencyStatus: emergency.emergencyStatus,
      emergencyInfo: emergency.emergencyInfo,
      emergencyTime: emergency.emergencyTime,
      emergencyTitle: emergency.emergencyTitle,
      emergencyType: emergency.emergencyType,
      emergencyStaffId: emergency.emergencyStaffId

    }
  })
}

export function getAllteachingTodo() {
  return request({
    url: '/teachingTodo/all',
    method: 'get',  
    params:{
      limit:'100',
      page:'1'
    }
  })
}

export function getteachingTodoById(id) {
  return request({
    url: '/teachingTodo/one',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function updateteachingTodo(teachingTodo) {
  return request({
    url: '/teachingTodo/update',
    method: 'POST',
    params: {
      teachingTodoId: teachingTodo.teachingTodoId, // 教学代办ID
      teachingTodoType: teachingTodo.teachingTodoType, // 事件类别
      teachingTodoExplain: teachingTodo.teachingTodoExplain, // 说明
      teachingTodoUrgency: teachingTodo.teachingTodoUrgency, // 紧急程度
      teachingTodoRelatedPersonnel: teachingTodo.teachingTodoRelatedPersonnel, // 相关人员
      teachingTodoInformationNote: teachingTodo.teachingTodoInformationNote, // 情况说明
      teachingTodoWorkArrangement: teachingTodo.teachingTodoWorkArrangement, // 工作安排
      teachingTodoImplementationInstructions: teachingTodo.teachingTodoImplementationInstructions, // 之星说明
      teachingTodoStageSummary: teachingTodo.teachingTodoStageSummary, // 阶段总结
      teachingTodoCompletionInstructions: teachingTodo.teachingTodoCompletionInstructions, // 完成说明
      teachingTodoFollowUpArrangements: teachingTodo.teachingTodoFollowUpArrangements// 后续安排

    }
  })
}

export function deleteemergency(id) {
  return request({
    url: '/emergency/',
    method: 'delete',
    params: {
      emergencyId: id
    }
  })
}

export function getSearchAttAll(emergency) {
  return request({

    url: '/emergency/selectByClassIdAndSKCS',
    method: 'get',
    params: {
      clazzSkcd: emergency.clazzSkcd,
      clazzId: emergency.classId
    }
  })
}
