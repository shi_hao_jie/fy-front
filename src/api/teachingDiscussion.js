import request from '@/utils/request'
import store from '@/store/index'

export function adddiscuss(plan,BB) {
  console.log(plan)
  var Participants_Str = plan.teachingDiscussionRelatedPersonnelIDs.split(";")
  var Participants_Int=[]
  for (let i = 0; i < Participants_Str.length-1; i++) {
    console.log(parseInt(Participants_Str[i]));
    Participants_Int.push(parseInt(Participants_Str[i]))
  }
  console.log(Participants_Int)
  var teachingDiscussion =
  {
    // userId: parseInt(data.username),
    // pwd: data.password
    teachingDiscussionClazzTypeId:plan.teachingDiscussionClazzTypeId,
    teachingDiscussionEventDescription:plan.teachingDiscussionEventDescription,
    teachingDiscussionEventSummary:plan.teachingDiscussionEventSummary,
    teachingDiscussionEventTitle:plan.teachingDiscussionEventTitle,
    teachingDiscussionEventType:plan.teachingDiscussionEventType,
    teachingDiscussionExecutionRecord:plan.teachingDiscussionExecutionRecord,
    teachingDiscussionId:0,
    teachingDiscussionPostTime:"",
    teachingDiscussionPostUserId:BB,
    teachingDiscussionPostUserName:"1",
    teachingDiscussionPrincipalTeacherId:plan.teachingDiscussionPrincipalTeacherId,
    teachingDiscussionPrincipalTeacherName:"1",
    teachingDiscussionRelatedPersonnelIDs:Participants_Int,
    teachingDiscussionRelatedPersonnelIdNames:[],
    teachingDiscussionSolution:plan.teachingDiscussionSolution,
    teachingDiscussionStatus:plan.teachingDiscussionStatus,
    teachingDiscussionTeacherId: BB,
    teachingDiscussionTeacherName:"",
    teachingDiscussionTeachingSupervisorId:plan.teachingDiscussionTeachingSupervisorId,
    teachingDiscussionTeachingSupervisorName:"",
    teachingDiscussionTechnicalTypeId:plan.teachingDiscussionTechnicalTypeId,
      // plansPeopleId: plans.plansPeopleId,
      // plansStudentId: plans.plansStudentId, // 学生姓名
      // plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      // plansLessonPlan: plans.plansLessonPlan, // 课程计划
      // plansStageGoal: plans.plansStageGoal, // 阶段目标
      // plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      // plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      // plansId: plans.plansId // 计划id

  }
  var planData = JSON.stringify(teachingDiscussion)
  console.log(planData);
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachingDiscussion/insert',
    method: 'post',
    data: planData
  })
}

export function addplans(plans) {
  return request({
    url: '/plans/one',
    method: 'post',
    params: {
      plansPeopleId: plans.plansPeopleId,
      plansStudentId: plans.plansStudentId, // 学生姓名
      plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      plansLessonPlan: plans.plansLessonPlan, // 课程计划
      plansStageGoal: plans.plansStageGoal, // 阶段目标
      plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      plansTeacherId: store.getters.userId
      // plansId: plans.plansId // 计划id

    }
  })
}

// export function adddiscuss(teachingDiscussion, BB) {
//   return request({
//     url: '/teachingDiscussion/insert',
//     method: 'post',
//     params: {
//       teachingDiscussionId: teachingDiscussion.teachingDiscussionId,
//       teachingDiscussionEventType: teachingDiscussion.teachingDiscussionEventType,
//       teachingDiscussionEventTitle: teachingDiscussion.teachingDiscussionEventTitle,
//       teachingDiscussionEventDescription: teachingDiscussion.teachingDiscussionEventDescription,
//       teachingDiscussionParticipants: teachingDiscussion.teachingDiscussionParticipants,
//       teachingDiscussionSolution: teachingDiscussion.teachingDiscussionSolution,
//       teachingDiscussionExecutionRecord: teachingDiscussion.teachingDiscussionExecutionRecord,
//       teachingDiscussionEventSummary: teachingDiscussion.teachingDiscussionEventSummary,
//       teachingDiscussionStatus: teachingDiscussion.teachingDiscussionStatus,

//       teachingDiscussionPrincipalTeacherId: teachingDiscussion.teachingDiscussionPrincipalTeacherId,
//       teachingDiscussionTeacherId: teachingDiscussion.teachingDiscussionTeacherId,
//       teachingDiscussionTeachingSupervisorId: teachingDiscussion.teachingDiscussionTeachingSupervisorId,
//       teachingDiscussionClazzTypeId: teachingDiscussion.teachingDiscussionClazzTypeId,
//       teachingDiscussionTechnicalTypeId: teachingDiscussion.teachingDiscussionTechnicalTypeId,
//       teachingDiscussionPostUserId: BB
//     }
//   })
// }

export function getAll() { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all',
    method: 'get',
    data:{
      limit:100,
      page:1
    }
  })
}

export function getAllteachingDiscussion(BB) { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachingDiscussionByDirector(BB) { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllteachingDiscussionAAA() { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all',
    method: 'get',
    params:{
      limit:1000,
      page:1
    }
  })
}

export function getAllteachingDiscussionByprincipal(BB) { // 获取所有教学讨论
  return request({
    url: '/teachingDiscussion/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}
export function getteachingDiscussionById(id) { // 根据id获取信息
  return request({
    url: '/teachingDiscussion/one',
    method: 'get',
    params: {
      id: id
    }
  })
}
export function getclazzplansById(id) {
  return request({
    url: 'plansClazz/one',
    method: 'get',
    params: {
      Id: id // 根据计划id获取班级计划
    }
  })
}
export function updateplans(plans) {
  return request({
    url: '/plans/',
    method: 'put',
    params: {
      plansPeopleId: plans.plansPeopleId,
      plansStudentId: plans.plansStudentId, // 学生姓名
      plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      plansLessonPlan: plans.plansLessonPlan, // 课程计划
      plansStageGoal: plans.plansStageGoal, // 阶段目标
      plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      plansTeacherId: store.getters.userId,
      plansId: plans.plansId // 计划id
    }
  })
}

export function updateplansByprincipal(plan) {
  console.log(plan)
  var Participants_Str = plan.teachingDiscussionParticipants.split(";")
  var Participants_Int=[]
  for (let i = 0; i < Participants_Str.length-1; i++) {
    console.log(parseInt(Participants_Str[i]));
    Participants_Int.push(parseInt(Participants_Str[i]))
  }
  console.log(Participants_Int)
  var teachingDiscussion =
  {
    // userId: parseInt(data.username),
    // pwd: data.password
    teachingDiscussionClazzTypeId:plan.teachingDiscussionClazzTypeId,
    teachingDiscussionEventDescription:plan.teachingDiscussionEventDescription,
    teachingDiscussionEventSummary:plan.teachingDiscussionEventSummary,
    teachingDiscussionEventTitle:plan.teachingDiscussionEventTitle,
    teachingDiscussionEventType:plan.teachingDiscussionEventType,
    teachingDiscussionExecutionRecord:plan.teachingDiscussionExecutionRecord,
    teachingDiscussionId:plan.teachingDiscussionId,
    teachingDiscussionPostTime:"",
    teachingDiscussionPostUserId:plan.teachingDiscussionTeacherId,
    teachingDiscussionPostUserName:"1",
    teachingDiscussionPrincipalTeacherId:plan.teachingDiscussionPrincipalTeacherId,
    teachingDiscussionPrincipalTeacherName:"1",
    teachingDiscussionRelatedPersonnelIDs:plan.teachingDiscussionRelatedPersonnelIDs,
    teachingDiscussionRelatedPersonnelIdNames:[],
    teachingDiscussionSolution:plan.teachingDiscussionSolution,
    teachingDiscussionStatus:plan.teachingDiscussionStatus,
    teachingDiscussionTeacherId: plan.teachingDiscussionTeacherId,
    teachingDiscussionTeacherName:"",
    teachingDiscussionTeachingSupervisorId:plan.teachingDiscussionTeachingSupervisorId,
    teachingDiscussionTeachingSupervisorName:"",
    teachingDiscussionTechnicalTypeId:plan.teachingDiscussionTechnicalTypeId,
      // plansPeopleId: plans.plansPeopleId,
      // plansStudentId: plans.plansStudentId, // 学生姓名
      // plansAdmissionAssessment: plans.plansAdmissionAssessment, // 入学评估
      // plansLessonPlan: plans.plansLessonPlan, // 课程计划
      // plansStageGoal: plans.plansStageGoal, // 阶段目标
      // plansTeachingPlan: plans.plansTeachingPlan, // 教学计划
      // plansTeachingScheme: plans.plansTeachingScheme, // 教学方案
      // plansId: plans.plansId // 计划id

  }
  var planData = JSON.stringify(teachingDiscussion)
  console.log(planData);
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/teachingDiscussion/update',
    method: 'post',
    data:
      planData,

    params:{
      teachingDiscussionId:plan.teachingDiscussionId
    }
  })
}

export function updateclazzplans(plans) { // 根据班级计划id修改
  return request({
    url: '/plansClazz/',
    method: 'put',
    params: {
      plansclazzStageGoal: plans.plansclazzStageGoal, // 阶段目标
      plansclazzId: plans.plansclazzId, // 班级计划id
      plansclazzTrainingPrograms: plans.plansclazzTrainingPrograms, // 训练项目
      plansclazzTeachingPreparation: plans.plansclazzTeachingPreparation, // 教学准备
      plansclazzMattersNeedingAttention: plans.plansclazzMattersNeedingAttention // 注意事项
      // 注意事项
    }
  })
}

export function deleteplans(id) {
  return request({
    url: '/plans/',
    method: 'delete',
    params: {
      plansId: id
    }
  })
}

export function getAllTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}

export function selectclazzType() {
  return request({
    url: '/clazzType/all',
    method: 'get'
  })
}

export function selectTechnicalType() {
  return request({
    url: '/technicalType/all',
    method: 'get'
  })
}
