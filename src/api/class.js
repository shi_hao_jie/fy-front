import request from '@/utils/request'

export function addClass(clazz) {
  const dfdsfsdf = {
    classCompletionTimes: clazz.classCompletionTimes,
    classLessonId: clazz.classLessonId,
    classMedicalDirectorId: clazz.classMedicalDirectorId,
    classPrincipalTeacherId: clazz.classPrincipalTeacherId,
    classStatus: clazz.classStatus,
    classTeacherId: clazz.classTeacherId,
    classTeachingSupervisorId: clazz.classTeachingSupervisorId,
    classTimes: clazz.classTimes,
    classTrainingMethods: clazz.classTrainingMethods,
    classType: clazz.classType,
    classEndTime: clazz.classEndTime,
    classStartTime: clazz.classStartTime,
    classStudentIDs: clazz.classStudentIDs,
    className: clazz.className
  }
  return request({
    url: '/clazz/one',
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(dfdsfsdf)
  })
}

export function getAllClass() {
  return request({
    url: '/clazz/all',
    method: 'get',
    params: {
      limit: 999,
      page: 1
    }
  })
}

export function getClassById(classId) {
  return request({
    url: '/clazz/id?classId=' + classId,
    method: 'get'

  })
}

export function updateClass(clazz) {
  return request({
    url: '/clazz/',
    method: 'put',
    params: {
      classCompletionTimes: clazz.classCompletionTimes,
      classLessonId: clazz.classLessonId,
      classId: clazz.classId,
      classMedicalDirectorId: clazz.classMedicalDirectorId,
      classResponsibleTeacherId: clazz.classResponsibleTeacherId,
      classStatus: clazz.classStatus,
      classTeacherId: clazz.classTeacherId,
      classTeachingDirectorId: clazz.classTeachingDirectorId,
      classTimes: clazz.classTimes,
      classTrainingMethods: clazz.classTrainingMethods,
      classType: clazz.classType,
      endTimeToString: clazz.endTimeToString,
      startTimeToString: clazz.startTimeToString,
      studentId: clazz.studentId,
      className: clazz.className
    }
  })
}

export function deleteClass(classId) {
  return request({
    url: '/clazz/?classId=' + classId,
    method: 'delete'

  })
}

export function ClassIdSearch(search) {
  return request({
    url: '/clazz/id',
    method: 'get',
    data: {
      classId: search.classId

      // userid: getToken()
    }
  })
}

export function judgement(className) {
  return request({
    url: '/clazz/judgement/className',
    method: 'get',
    params: {
      className: className
    }
  })
}

export function updateJudgement(classId, className) {
  return request({
    url: '/clazz/judgement/update/className',
    method: 'get',
    params: {
      classId: classId,
      className: className
    }
  })
}
