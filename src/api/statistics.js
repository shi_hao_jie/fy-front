import request from '@/utils/request'

export function moneyStatistics() {
  return request({
    url: '/count/countAllStudentPayInMonth',
    method: 'get'
  })
}

export function remainClassStatistics() {
  return request({
    url: '/count/countAllStudentRemainingClassHoursStatisticsInMonth',
    method: 'get'
  })
}

export function teaWorkloadStatistics() {
  return request({
    url: '/count/countAllTeacherWorkingConditionsInMonth',
    method: 'get'
  })
}

export function eduManageCountStatistics() {
  return request({
    url: '/count/countAllTeachingManagementTimesStatisticsInMonth',
    method: 'get'
  })
}

export function stuStatistics() {
  return request({
    url: '/count/countAllStudentStatusStatisticsInMonth',
    method: 'get'
  })
}
