import request from '@/utils/request'
import store from '@/store/index'

export function getAll() {
  return request({
    url: '/teachingTodo/all',
    method: 'GET',
    params:{
      limit:'100',
      page:'1'
    }
  })
}

export function getTeachTodoById(id) {
  return request({
    url: '/teachingTodo/one',
    method: 'GET',
    params: {
      id: id
    }
  })
}

export function deleteTeachTodo(id) {
  return request({
    url: '/teachingTodo/delete',
    method: 'POST',
    params: {
      id: id
    }
  })
}

export function addTeachTodo(teachTodo) {
  const sendTeachingTodo={
    teachingTodoId: teachTodo.teachingTodoId,
    teachingTodoCompletionInstructions: teachTodo.teachingTodoCompletionInstructions,
    teachingTodoExplain: teachTodo.teachingTodoExplain,
    teachingTodoFollowUpArrangements: teachTodo.teachingTodoFollowUpArrangements,
    teachingTodoImplementationInstructions: teachTodo.teachingTodoImplementationInstructions,
    teachingTodoInformationNote: teachTodo.teachingTodoInformationNote,
    teachingTodoPostUserId: store.getters.userId,
    teachingTodoPostUserName:store.getters.name,
    teachingTodoRelatedPersonnelIDs:teachTodo.teachingTodoRelatedPersonnelIDs,
    teachingTodoStageSummary: teachTodo.teachingTodoStageSummary,
    teachingTodoStatus: teachTodo.teachingTodoStatus,
    teachingTodoType: teachTodo.teachingTodoType,
    teachingTodoUrgency: teachTodo.teachingTodoUrgency,
    teachingTodoWorkArrangement: teachTodo.teachingTodoWorkArrangement
  }
  return request({
    url: '/teachingTodo/insert',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(sendTeachingTodo),
  })
}

export function updateTeachTodo(teachTodo) {
  const sendTeachingTodo={
    teachingTodoId: teachTodo.teachingTodoId,
    teachingTodoCompletionInstructions: teachTodo.teachingTodoCompletionInstructions,
    teachingTodoExplain: teachTodo.teachingTodoExplain,
    teachingTodoFollowUpArrangements: teachTodo.teachingTodoFollowUpArrangements,
    teachingTodoImplementationInstructions: teachTodo.teachingTodoImplementationInstructions,
    teachingTodoInformationNote: teachTodo.teachingTodoInformationNote,
    teachingTodoPostUserId: store.getters.userId,
    teachingTodoPostUserName:store.getters.name,
    teachingTodoRelatedPersonnelIDs:teachTodo.teachingTodoRelatedPersonnelIDs,
    teachingTodoStageSummary: teachTodo.teachingTodoStageSummary,
    teachingTodoStatus: teachTodo.teachingTodoStatus,
    teachingTodoType: teachTodo.teachingTodoType,
    teachingTodoUrgency: teachTodo.teachingTodoUrgency,
    teachingTodoWorkArrangement: teachTodo.teachingTodoWorkArrangement
  }
  return request({
    url: '/teachingTodo/update',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(sendTeachingTodo),
    params:{
      teachingTodoId:teachTodo.teachingTodoId
    }
  })
}
