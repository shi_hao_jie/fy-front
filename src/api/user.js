import request from '@/utils/request'
export function login(data) {
  var adminLoginRequestBody =
  {
    userId: parseInt(data.username),
    pwd: data.password
  }
  var loginData = JSON.stringify(adminLoginRequestBody)
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/user/admin/login',
    method: 'post',
    data: loginData
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token: token }
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}
