import request from '@/utils/request'

export function addsupervisor(supervisor) {
  return request({
    url: '/supervisor/one',
    method: 'post',
    params: {
      supervisorStudentId: supervisor.supervisorStudentId, // 学生姓名
      supervisorAdmission: supervisor.supervisorAdmission, // 入学评审
      supervisorComplete: supervisor.supervisorComplete, // 完成评审
      supervisorPhase: supervisor.supervisorPhase, // 阶段评审
      supervisorPlan: supervisor.supervisorPlan, // 计划评审
      supervisorScheme: supervisor.supervisorScheme, // 方案评审
      supervisorStaffId: supervisor.supervisorStaffId, //督导老师id	
      supervisorTeacherId: supervisor.supervisorTeacherId, //授课教师id	
      supervisorLessonId: supervisor.supervisorLessonId	, //课程id
      supervisorRoomId: supervisor.supervisorRoomId, //教室id	
      supervisorTime: supervisor.supervisorTime, //督导时间	

    }
  })
}

export function getAllsupervisor() {
  return request({
    url: '/supervisor/all',
    method: 'get'
  })
}

export function getsupervisorById(id) {
  return request({
    url: '/supervisor/one',
    method: 'get',
    params: {
      supervisorId: id
    }
  })
}

export function updatesupervisor(supervisor) {
  return request({
    url: '/supervisor/',
    method: 'put',
    params: {
      supervisorStudentId: supervisor.supervisorStudentId, // 学生姓名
      supervisorAdmission: supervisor.supervisorAdmission, // 入学评审
      supervisorComplete: supervisor.supervisorComplete, // 完成评审
      supervisorPhase: supervisor.supervisorPhase, // 阶段评审
      supervisorPlan: supervisor.supervisorPlan, // 计划评审
      supervisorScheme: supervisor.supervisorScheme, // 方案评审
      supervisorId: supervisor.supervisorId, // id
      supervisorStaffId: supervisor.supervisorStaffId, //督导老师id	
      supervisorTeacherId: supervisor.supervisorTeacherId, //授课教师id	
      supervisorLessonId: supervisor.supervisorLessonId	, //课程id
      supervisorRoomId: supervisor.supervisorRoomId, //教室id	
      supervisorTime: supervisor.supervisorTime, //督导时间	
    }
  })
}

export function deletesupervisor(id) {
  return request({
    url: '/supervisor/',
    method: 'delete',
    params: {
        supervisorId: id
    }
  })
}

