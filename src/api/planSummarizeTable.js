import request from '@/utils/request'

export function addplanSummarizeTable(plan) {
  return request({
    url: '/planSummarizeTable/one/',
    method: 'post',
    params: {
      planSummarizeTableAreaTypeId:plan.plan.planSummarizeTable.planSummarizeTableAreaTypeId,
        planSummarizeTableFinishstandard:plan.plan.planSummarizeTable.planSummarizeTableFinishstandard,
        planSummarizeTableFinishtimeToString:plan.plan.planSummarizeTable.planSummarizeTableFinishtimeToString,
        planSummarizeTableId:plan.plan.planSummarizeTable.planSummarizeTableId,
        planSummarizeTableStarttimeToString:plan.plan.planSummarizeTable.planSummarizeTableStarttimeToString,
        planSummarizeTableTeachactivity:plan.plan.planSummarizeTable.planSummarizeTableTeachactivity,
        planSummarizeTableTeachprogram:plan.plan.planSummarizeTable.planSummarizeTableTeachprogram

    }
  })
}

export function getAllplanSummarizeTable() {
  return request({
    url: '/planSummarizeTable/all/',
    method: 'get'
  })
}

export function getplanSummarizeTableById(id) {
  return request({
    url: '/planSummarizeTable/one',
    method: 'get',
    params: {
        planSummarize: id
    }
  })
}

export function updateplanSummarizeTable(plan) {
  return request({
    url: '/planSummarizeTable/',
    method: 'put',
    params: {
        planSummarizeTableAreaTypeId:plan.plan.planSummarizeTable.planSummarizeTableAreaTypeId,
        planSummarizeTableFinishstandard:plan.plan.planSummarizeTable.planSummarizeTableFinishstandard,
        planSummarizeTableFinishtimeToString:plan.plan.planSummarizeTable.planSummarizeTableFinishtimeToString,
        planSummarizeTableId:plan.plan.planSummarizeTable.planSummarizeTableId,
        planSummarizeTableStarttimeToString:plan.plan.planSummarizeTable.planSummarizeTableStarttimeToString,
        planSummarizeTableTeachactivity:plan.plan.planSummarizeTable.planSummarizeTableTeachactivity,
        planSummarizeTableTeachprogram:plan.plan.planSummarizeTable.planSummarizeTableTeachprogram
    }
  })
}

export function deleteplanSummarizeTable(id) {
  return request({
    url: '/planSummarizeTable/',
    method: 'delete',
    params: {
      planId: id
    }
  })
}
