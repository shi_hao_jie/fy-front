import request from '@/utils/request'

export function addexecutiveCondition(executiveCondition) {
  return request({
    url: '/executiveCondition/one',
    method: 'post',
    params: {
      executiveConditionSummarize: executiveCondition.executiveConditionSummarize, // 课后小结
      executiveConditionStudentId: executiveCondition.executiveConditionStudentId, // 学生
      executiveConditionTeacherId: executiveCondition.executiveConditionTeacherId,
      executiveConditionPeriodchat: executiveCondition.executiveConditionPeriodchat, // 阶段讨论
      executiveConditionPrepare: executiveCondition.executiveConditionPrepare, // 课前准备
      executiveConditionRecord: executiveCondition.executiveConditionRecord, // 课堂记录
      executiveConditionTeachchat: executiveCondition.executiveConditionTeachchat, // 教学讨论
      executiveConditionTime: executiveCondition.executiveConditionTime

    }
  })
}

export function getAllexecutiveCondition() {
  return request({
    url: '/executiveCondition/all',
    method: 'get'
  })
}

export function getexecutiveConditionById(id) {
  return request({
    url: '/executiveCondition/one',
    method: 'get',
    params: {
      executiveConditionId: id
    }
  })
}

export function updateexecutiveCondition(executiveCondition) {
  return request({
    url: '/executiveCondition/',
    method: 'put',
    params: {
      executiveConditionId :executiveCondition.executiveConditionId,
      executiveConditionSummarize: executiveCondition.executiveConditionSummarize, // 课后小结
      executiveConditionStudentId: executiveCondition.executiveConditionStudentId, // 学生
      executiveConditionTeacherId: executiveCondition.executiveConditionTeacherId,
      executiveConditionPeriodchat: executiveCondition.executiveConditionPeriodchat, // 阶段讨论
      executiveConditionPrepare: executiveCondition.executiveConditionPrepare, // 课前准备
      executiveConditionRecord: executiveCondition.executiveConditionRecord, // 课堂记录
      executiveConditionTeachchat: executiveCondition.executiveConditionTeachchat, // 教学讨论
      executiveConditionTime: executiveCondition.executiveConditionTime
    }
  })
}

export function deleteexecutiveCondition(id) {
  return request({
    url: '/executiveCondition/',
    method: 'delete',
    params: {
      executiveConditionId: id
    }
  })
}
