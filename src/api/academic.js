import request from '@/utils/request'

export function updateacademic(academic) {
  return request({
    url: '/academic/',
    method: 'put',
    params: {
      academicAchievement: academic.academicAchievement, // 学术成果
      academicCompleteeva: academic.academicCompleteeva, // 学术完成评价
      academicId: academic.academicId, // 学术id
      academicImprovementeva: academic.academicImprovementeva, // 学术改进评价
      academicMeaning: academic.academicMeaning, // 学术意义
      academicName: academic.academicName, // 学术名称
      academicPersonId: academic.academicPersonId, // 学术责任人id
      academicPhasesum: academic.academicPhasesum, // 学术阶段总结
      academicSchedule: academic.academicSchedule, // 学术执行进度
      academicTarget: academic.academicTarget // 学术目标
    }
  })
}

export function deleteacademic(id) {
  return request({
    url: '/academic/',
    method: 'delete',
    params: {
      academicId: id
    }
  })
}
export function getAllacademic() {
  return request({
    url: '/academic/type',
    method: 'get',
    params: {
      academicType: 1
    }
  })
}
export function getAllapplication() {
  return request({
    url: '/academic/type',
    method: 'get',
    params: {
      academicType: 2
    }
  })
}
export function getAllachievement() {
  return request({
    url: '/academic/type',
    method: 'get',
    params: {
      academicType: 3
    }
  })
}
export function getacademicById(id) {
  return request({
    url: '/academic/one',
    method: 'get',
    params: {
      academicId: id
    }
  })
}

export function addacademic(academic) {
  return request({
    url: '/academic/one',
    method: 'post',
    params: {
      academicAchievement: academic.academicAchievement, // 学术成果
      academicCompleteeva: academic.academicCompleteeva, // 学术完成评价
      academicImprovementeva: academic.academicImprovementeva, // 学术改进评价
      academicMeaning: academic.academicMeaning, // 学术意义
      academicName: academic.academicName, // 学术名称
      academicPersonId: academic.academicPersonId, // 学术责任人id
      academicPhasesum: academic.academicPhasesum, // 学术阶段总结
      academicSchedule: academic.academicSchedule, // 学术执行进度
      academicTarget: academic.academicTarget, // 学术目标
      academicType: 1
    }
  })
}

export function addapplication(academic) {
  return request({
    url: '/academic/one',
    method: 'post',
    params: {
      academicAchievement: academic.academicAchievement, // 学术成果
      academicCompleteeva: academic.academicCompleteeva, // 学术完成评价
      academicImprovementeva: academic.academicImprovementeva, // 学术改进评价
      academicMeaning: academic.academicMeaning, // 学术意义
      academicName: academic.academicName, // 学术名称
      academicPersonId: academic.academicPersonId, // 学术责任人id
      academicPhasesum: academic.academicPhasesum, // 学术阶段总结
      academicSchedule: academic.academicSchedule, // 学术执行进度
      academicTarget: academic.academicTarget, // 学术目标
      academicType: 2
    }
  })
}

export function addachievement(academic) {
  return request({
    url: '/academic/one',
    method: 'post',
    params: {
      academicAchievement: academic.academicAchievement, // 学术成果
      academicCompleteeva: academic.academicCompleteeva, // 学术完成评价
      academicImprovementeva: academic.academicImprovementeva, // 学术改进评价
      academicMeaning: academic.academicMeaning, // 学术意义
      academicName: academic.academicName, // 学术名称
      academicPersonId: academic.academicPersonId, // 学术责任人id
      academicPhasesum: academic.academicPhasesum, // 学术阶段总结
      academicSchedule: academic.academicSchedule, // 学术执行进度
      academicTarget: academic.academicTarget, // 学术目标
      academicType: 3
    }
  })
}
