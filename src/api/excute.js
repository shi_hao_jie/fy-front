import request from '@/utils/request'

export function addexcute(excute) {
  return request({
    url: '/excute/one',
    method: 'post',
    params: {
      excuteStudentId: excute.excuteStudentId, // 儿童姓名
      excuteDaytime: excute.excuteDaytime, // 天数
      excuteExpression: excute.excuteExpression, // 儿童表现
      excuteIssue: excute.excuteIssue, // 问题及解决方案
      excutePlantarget: excute.excutePlantarget, // 计划目标
      excuteProject: excute.excuteProject, // 教学项目
      excuteStarttime: excute.excuteStarttime, // 开始时间
      excuteSummarize: excute.excuteSummarize, // 总结
      excuteTeacherId: excute.excuteTeacherId, // 执行教师
      excuteTeachtarget: excute.excuteTeachtarget // 教学目标

    }
  })
}

export function getAllexcute() {
  return request({
    url: '/excute/all',
    method: 'get'
  })
}

export function getexcuteById(id) {
  return request({
    url: '/excute/one',
    method: 'get',
    params: {
      excuteId: id
    }
  })
}

export function updateexcute(excute) {
  return request({
    url: '/excute/',
    method: 'put',
    params: {
      excuteId: excute.excuteId,
      excuteStudentId: excute.excuteStudentId, // 儿童姓名
      excuteDaytime: excute.excuteDaytime, // 天数
      excuteExpression: excute.excuteExpression, // 儿童表现
      excuteIssue: excute.excuteIssue, // 问题及解决方案
      excutePlantarget: excute.excutePlantarget, // 计划目标
      excuteProject: excute.excuteProject, // 教学项目
      excuteStarttime: excute.excuteStarttime, // 开始时间
      excuteSummarize: excute.excuteSummarize, // 总结
      excuteTeacherId: excute.excuteTeacherId, // 执行教师
      excuteTeachtarget: excute.excuteTeachtarget // 教学目标

    }
  })
}

export function deleteexcute(id) {
  return request({
    url: '/excute/',
    method: 'delete',
    params: {
      excuteId: id
    }
  })
}
