import request from '@/utils/request'
import store from '@/store/index'

export function addPay(pay) {
  return request({
    url: '/pay/one',
    method: 'post',
    data: {
      classHours: pay.classHours,
      lessonId: pay.lessonId,
      parentId: pay.parentId,
      paySubtotal: pay.paySubtotal,
      payType: pay.payType,
      staffId: store.getters.userId,
      studentId: pay.studentId,
      unitPrice: pay.unitPrice,
      remindTimes: pay.remindTimes

    }
  })
}

export function getAllPay() {
  return request({
    url: '/pay/all',
    method: 'get'
  })
}

export function getPayById(id) {
  return request({
    url: '/pay/',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function updatePay(pay) {
  return request({
    url: '/pay/',
    method: 'put',
    params: {
      payId: pay.payId,
      classHours: pay.classHours,
      lessonId: pay.lessonId,
      parentId: pay.parentId,
      paySubtotal: pay.paySubtotal,
      payType: pay.payType,
      staffId: store.getters.userId,
      unitPrice: pay.unitPrice
    }
  })
}

export function deletePay(id) {
  return request({
    url: '/pay/',
    method: 'delete',
    params: {
      id: id
    }
  })
}
