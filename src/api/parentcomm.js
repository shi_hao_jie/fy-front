import request from "@/utils/request";

export function updateParentComm(parentcomm) {
  return request({
    url: "/parentcomm/",
    method: "put",
    params: {
        PCommId:parentcomm.pcommId,
        PCommInfo: parentcomm.pcommInfo, //沟通内容
        PCreattime: parentcomm.pCreattime, //创建时间
        PInfo: parentcomm.pinfo, //处理建议
        PParentInfo: parentcomm.pparentInfo, //家长问题及建议
        PParentRelation: parentcomm.pparentRelation, //儿童关系
        PParentid: parentcomm.pparentid , //家长id
        PStaffid: parentcomm.pstaffid, //主责老师
        PStaffidDirector: parentcomm.pstaffidDirector, //教学主管id
        PStuid: parentcomm.pstuid, //学生id
        pattendingdoctorId:parentcomm.pattendingdoctorId, //主治医师id
        PIssue:parentcomm.pissue,//事件描述
        PMeetingplaceId:parentcomm.pmeetingplaceId//地点id
    }
  });
}

export function deleteParentComm(id) {
  return request({
    url: "/parentcomm/",
    method: "delete",
    params: {
        parentCommId: id
    }
  });
}

export function getAllParentComm() {
  return request({
    url: "/parentcomm/all",
    method: "get"
  });
}

export function getParentCommbyId(id) {
  return request({
    url: "/parentcomm/select",
    method: "get",
    params: {
        parentCommId: id
    }
  });
}

export function addoneParentComm(parentcomm) {
  return request({
    url: "/parentcomm/insert",
    method: "post",
    data: {
        PCommId: parentcomm.PCommId, //家长沟通记录id
        PCommInfo: parentcomm.pcommInfo, //沟通内容
        PCreattime: parentcomm.pCreattime, //创建时间
        PInfo: parentcomm.pinfo, //处理建议
        PParentInfo: parentcomm.pparentInfo, //家长问题及建议
        PParentRelation: parentcomm.pparentRelation, //儿童关系
        PParentid: parentcomm.pparentid , //家长id
        PStaffid: parentcomm.pstaffid, //主责老师
        PStaffidDirector: parentcomm.pstaffidDirector, //教学主管id
        PStuid: parentcomm.pstuid, //学生id
        PAttendingdoctorId:parentcomm.pattendingdoctorId, //主治医师id
        PIssue:parentcomm.pissue, //事件描述
        PMeetingplaceId:parentcomm.pmeetingplaceId//会议地点id
    }
  });
}

export function getTeacherString(id) {
  return request({
    url: "/staff/selectStaffnameAndStaffId/ByJurisdiction",
    method: "get",
    params: {
        Jurisdiction: id
    }
  });
}
  
export function getClassType() {
  return request({
    url: "/clazzType/one",
    method: "get"
  });
}

export function getAllClassRoom() {
  return request({
    url: "/room/all",
    method: "get"
  });
}

export function getAllstudentName() {
  return request({
    url: "/student/selectStudentNameAndAdd",
    method: "get"
  });
}

export function getAllparentName() {
  return request({
    url: "/parent/selectParentNameAndCall",
    method: "get"
  });
}
