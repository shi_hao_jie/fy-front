import request from '@/utils/request'

export function updateRecordsById(Records) {
  return request({
    url: '/purchaseCourseRecords/',
    method: 'put',
    params: {
      classStatus: Records.classStatus, // 课程状态 0：未完成 1：已完成
      classTimes: Records.classTimes, // 使用课程次数
      lessonId: Records.lessonId, // 购买课程id
      payId: Records.payId, // 缴费记录id
      purchaseId: Records.purchaseId, // 购买课程记录id
      purchaseTimes: Records.purchaseTimes, // 购买课程次数
      remainingTimes: Records.remainingTimes, // 剩余次数
      studentId: Records.studentId // 学生id
    }
  })
}

export function getRecordsbyId(id) {
  return request({
    url: '/purchaseCourseRecords/one',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function getAllRecords() {
  return request({
    url: '/purchaseCourseRecords/all',
    method: 'get'
  })
}

export function getStudentString() {
  return request({
    url: '/student/selectStudentNameAndAdd',
    method: 'get'
  })
}
