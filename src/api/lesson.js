import request from '@/utils/request'

export function getAllLesson() {
  return request({
    url: '/lesson/all',
    method: 'get'
  })
}

export function getLessonById(id) {
  return request({
    url: '/lesson/id',
    method: 'get',
    params: {
      lessonId: id
    }
  })
}

export function getLessonType() {
  return request({
    url: '/clazzType/all',
    method: 'get'
  })
}
export function deleteLesson(id) {
  return request({
    url: '/lesson/',
    method: 'delete',
    params: {
      lessonId: id
    }
  })
}

export function addLesson(lesson) {
  return request({
    url: '/lesson/one',
    method: 'post',
    data: {
      lessonName: lesson.lessonName,
      lessonDuration: lesson.lessonDuration,
      lessonEffect: lesson.lessonEffect,
      lessonInfo: lesson.lessonInfo,
      lessonPic: lesson.lessonPic,
      lessonPrice: lesson.lessonPrice,
      lessonType: lesson.lessonType
    }
  })
}

export function updateLesson(lesson) {
  return request({
    url: '/lesson/',
    method: 'put',
    params: {
      lessonId: lesson.lessonId,
      lessonName: lesson.lessonName,
      lessonDuration: lesson.lessonDuration,
      lessonEffect: lesson.lessonEffect,
      lessonInfo: lesson.lessonInfo,
      lessonPic: lesson.lessonPic,
      lessonPrice: lesson.lessonPrice,
      lessonType: lesson.lessonType
    }
  })
}
