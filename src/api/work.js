import request from '@/utils/request'
import store from '@/store/index'

export function GetInfo(BB) {
  return request({
    url: '/count/countTeacherIdWorkingConditions',
    method: 'get',
    params: {
      teacherId: BB
    }
  })
}
