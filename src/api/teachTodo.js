import request from '@/utils/request'
import store from '@/store/index'

export function getAll() {
  return request({
    url: '/teachingTodo/all',
    method: 'GET',
    params:{
      limit:'100',
      page:'1'
    }
  })
}

export function getTeachTodoById(id) {
  return request({
    url: '/teachingTodo/one',
    method: 'GET',
    params: {
      id: id
    }
  })
}

export function deleteTeachTodo(id) {
  return request({
    url: '/teachingTodo/',
    method: 'DELETE',
    params: {
      id: id
    }
  })
}

export function addTeachTodo(teachTodo) {
  return request({
    url: '/teachingTodo/insert',
    method: 'POST',
    data: {
      teachingTodoCompletionInstructions: teachTodo.teachingTodoCompletionInstructions,
      teachingTodoExplain: teachTodo.teachingTodoExplain,
      teachingTodoFollowUpArrangements: teachTodo.teachingTodoFollowUpArrangements,
      teachingTodoImplementationInstructions: teachTodo.teachingTodoImplementationInstructions,
      teachingTodoInformationNote: teachTodo.teachingTodoInformationNote,
      teachingTodoPostUserId: store.getters.userId,
      teachingTodoRelatedPersonnel: teachTodo.teachingTodoRelatedPersonnel,
      teachingTodoStageSummary: teachTodo.teachingTodoStageSummary,
      teachingTodoStatus: teachTodo.teachingTodoStatus,
      teachingTodoType: teachTodo.teachingTodoType,
      teachingTodoUrgency: teachTodo.teachingTodoUrgency,
      teachingTodoWorkArrangement: teachTodo.teachingTodoWorkArrangement
    }
  })
}

export function updateTeachTodo(teachTodo) {
  return request({
    url: '/teachingTodo/update',
    method: 'POST',
    data: {
      teachingTodoId: teachTodo.teachingTodoId,
      teachingTodoCompletionInstructions: teachTodo.teachingTodoCompletionInstructions,
      teachingTodoExplain: teachTodo.teachingTodoExplain,
      teachingTodoFollowUpArrangements: teachTodo.teachingTodoFollowUpArrangements,
      teachingTodoImplementationInstructions: teachTodo.teachingTodoImplementationInstructions,
      teachingTodoInformationNote: teachTodo.teachingTodoInformationNote,
      teachingTodoPostUserId: store.getters.userId,
      teachingTodoRelatedPersonnel: teachTodo.teachingTodoRelatedPersonnel,
      teachingTodoStageSummary: teachTodo.teachingTodoStageSummary,
      teachingTodoStatus: teachTodo.teachingTodoStatus,
      teachingTodoType: teachTodo.teachingTodoType,
      teachingTodoUrgency: teachTodo.teachingTodoUrgency,
      teachingTodoWorkArrangement: teachTodo.teachingTodoWorkArrangement
    }
  })
}
