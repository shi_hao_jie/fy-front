import request from '@/utils/request'

export function updateRecords(records) {
  return request({
    url: '/record/',
    method: 'put',
    params: {
      recordId: records.recordId, // 听课记录id
      recordStudentId: records.recordStudentId, // 学生id
      recordClassroomId: records.recordClassroomId, // 教室id
      recordGuildsuggestion: records.recordGuildsuggestion, // 指导建议
      recordLessonId: records.recordLessonId, // 课程id
      recordListenTimeToString: records.recordListenTimeToString, // 听课日期
      recordMainteacherId: records.recordMainteacherId, // 主课教师id
      recordSuperviseteacherId: records.recordSuperviseteacherId, // 督导教师id
      recordTeachadvantage: records.recordTeachadvantage, // 教学优点
      recordTeachcontent: records.recordTeachcontent, // 教学内容记录
      recordTeachquestion: records.recordTeachquestion // 教学问题
    }
  })
}
export function updatesupervisor(plan) {
  console.log(plan)
  var Participants_Str = plan.instructionalSupervisionRelatedPersonnelIDs.split(";")
  var Participants_Int = []
  for (let i = 0; i < Participants_Str.length - 1; i++) {
    console.log(parseInt(Participants_Str[i]));
    Participants_Int.push(parseInt(Participants_Str[i]))
  }
  console.log(Participants_Int)
  var instructionalSupervision =
  {
    instructionalSupervisionClazzTypeId: plan.instructionalSupervisionClazzTypeId,
    instructionalSupervisionEventDescription: plan.instructionalSupervisionEventDescription,
    instructionalSupervisionEventSummary: plan.instructionalSupervisionEventSummary,
    instructionalSupervisionEventTitle: plan.instructionalSupervisionEventTitle,
    instructionalSupervisionEventType: plan.instructionalSupervisionEventType,
    instructionalSupervisionExecutionRecord: plan.instructionalSupervisionExecutionRecord,
    instructionalSupervisionId: plan.instructionalSupervisionId,
    instructionalSupervisionPostTime: plan.instructionalSupervisionPostTime,
    instructionalSupervisionPostUserId: plan.instructionalSupervisionPostUserId,
    instructionalSupervisionPostUserName: "0",
    instructionalSupervisionPrincipalTeacherId: plan.instructionalSupervisionPrincipalTeacherId,
    instructionalSupervisionPrincipalTeacherName: "0",
    instructionalSupervisionRelatedPersonnelIDs: Participants_Int,
    instructionalSupervisionRelatedPersonnelNames: [],
    instructionalSupervisionSolution: plan.instructionalSupervisionSolution,
    instructionalSupervisionStatus:plan.instructionalSupervisionStatus,
    instructionalSupervisionTeacherId: plan.instructionalSupervisionTeacherId,
    instructionalSupervisionTeacherName: "0",
    instructionalSupervisionTeachingSupervisorId: plan.instructionalSupervisionTeachingSupervisorId,
    instructionalSupervisionTeachingSupervisorName: "",
    instructionalSupervisionTechnicalTypeId: plan.instructionalSupervisionTechnicalTypeId

  }
  var planData = JSON.stringify(instructionalSupervision)
  console.log(planData);
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/instructionalSupervision/update',
    method: 'post',
    data:
      planData,
      params:{
        instructionalSupervisionId:plan.instructionalSupervisionId
      }
  })
}
// export function updatesupervisor(instructionalSupervision) {
//   return request({
//     url: '/instructionalSupervision/update',
//     method: 'post',
//     params: {
//       instructionalSupervisionEventType: instructionalSupervision.instructionalSupervisionEventType,
//       instructionalSupervisionEventTitle: instructionalSupervision.instructionalSupervisionEventTitle,
//       instructionalSupervisionEventDescription: instructionalSupervision.instructionalSupervisionEventDescription,
//       instructionalSupervisionParticipants: instructionalSupervision.instructionalSupervisionParticipants,
//       instructionalSupervisionSolution: instructionalSupervision.instructionalSupervisionSolution,
//       instructionalSupervisionExecutionRecord: instructionalSupervision.instructionalSupervisionExecutionRecord,
//       instructionalSupervisionEventSummary: instructionalSupervision.instructionalSupervisionEventSummary,
//       instructionalSupervisionStatus: instructionalSupervision.instructionalSupervisionStatus,
//       instructionalSupervisionId: instructionalSupervision.instructionalSupervisionId,
//       instructionalSupervisionClazzTypeId: instructionalSupervision.instructionalSupervisionClazzTypeId,
//       instructionalSupervisionTechnicalTypeId: instructionalSupervision.instructionalSupervisionTechnicalTypeId,
//       instructionalSupervisionTeacherId: instructionalSupervision.instructionalSupervisionTeacherId,
//       instructionalSupervisionPrincipalTeacherId: instructionalSupervision.instructionalSupervisionPrincipalTeacherId,
//       instructionalSupervisionTeachingSupervisorId: instructionalSupervision.instructionalSupervisionTeachingSupervisorId
//     }
//   })
// }
export function deleteRecords(id) {
  return request({
    url: '/record/',
    method: 'delete',
    params: {
      recordId: id
    }
  })
}

export function getAll() {
  return request({
    url: '/instructionalSupervision/all',
    method: 'get',
    params:{
      limit:1000,
      page:1
    }
  })
}

export function selectClazzType() {
  return request({
    url: 'clazzType/all',
    method: 'get'
  })
}
export function selectTechnicalType() {
  return request({
    url: 'technicalType/all',
    method: 'get'
  })
}
export function getAllRecords(BB) {
  return request({
    url: '/instructionalSupervision/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllRecordsA(BB) {
  return request({
    url: '/instructionalSupervision/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllRecordsByprincipal(BB) {
  return request({
    url: '/instructionalSupervision/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllRecordsAAAA() {
  return request({
    url: '/instructionalSupervision/all',
    method: 'get',
    params:{
      limit:1000,
      page:1
    }

  })
}
export function getRecordsbyId(id) {
  return request({
    url: '/instructionalSupervision/one',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function addoneRecord(records) {
  return request({
    url: '/record/one',
    method: 'post',
    data: {
      recordId: records.recordId, // 听课记录id
      recordStudentId: records.recordStudentId, // 学生id
      recordClassroomId: records.recordClassroomId, // 教室id
      recordGuildsuggestion: records.recordGuildsuggestion, // 指导建议
      recordLessonId: records.recordLessonId, // 课程id
      recordMainteacherId: records.recordMainteacherId, // 主课教师id
      recordSuperviseteacherId: records.recordSuperviseteacherId, // 督导教师id
      recordTeachadvantage: records.recordTeachadvantage, // 教学优点
      recordTeachcontent: records.recordTeachcontent, // 教学内容记录
      recordTeachquestion: records.recordTeachquestion, // 教学问题
      recordListenTimeToString: records.recordListentime // 听课日期
    }
  })
}

export function addsupervisor(plan, BB) {
  console.log(plan)
  var Participants_Str = plan.instructionalSupervisionRelatedPersonnelIDs.split(";")
  var Participants_Int = []
  for (let i = 0; i < Participants_Str.length - 1; i++) {
    console.log(parseInt(Participants_Str[i]));
    Participants_Int.push(parseInt(Participants_Str[i]))
  }
  console.log(Participants_Int)
  var instructionalSupervision =
  {
    instructionalSupervisionClazzTypeId: plan.instructionalSupervisionClazzTypeId,
    instructionalSupervisionEventDescription: plan.instructionalSupervisionEventDescription,
    instructionalSupervisionEventSummary: plan.instructionalSupervisionEventSummary,
    instructionalSupervisionEventTitle: plan.instructionalSupervisionEventTitle,
    instructionalSupervisionEventType: plan.instructionalSupervisionEventType,
    instructionalSupervisionExecutionRecord: plan.instructionalSupervisionExecutionRecord,
    instructionalSupervisionId: plan.instructionalSupervisionId,
    instructionalSupervisionPostTime: plan.instructionalSupervisionPostTime,
    instructionalSupervisionPostUserId: BB,
    instructionalSupervisionPostUserName: "0",
    instructionalSupervisionPrincipalTeacherId: plan.instructionalSupervisionPrincipalTeacherId,
    instructionalSupervisionPrincipalTeacherName: "0",
    instructionalSupervisionRelatedPersonnelIDs: Participants_Int,
    instructionalSupervisionRelatedPersonnelNames: [],
    instructionalSupervisionSolution: plan.instructionalSupervisionSolution,
    instructionalSupervisionStatus:plan.instructionalSupervisionStatus,
    instructionalSupervisionTeacherId: plan.instructionalSupervisionTeacherId,
    instructionalSupervisionTeacherName: "0",
    instructionalSupervisionTeachingSupervisorId: plan.instructionalSupervisionTeachingSupervisorId,
    instructionalSupervisionTeachingSupervisorName: "",
    instructionalSupervisionTechnicalTypeId: plan.instructionalSupervisionTechnicalTypeId

  }
  var planData = JSON.stringify(instructionalSupervision)
  console.log(planData);
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/instructionalSupervision/insert',
    method: 'post',
    data:
      planData,
  })
}

export function getTeacherString(id) {
  return request({
    url: '/staff/selectStaffnameAndStaffId/ByJurisdiction',
    method: 'get',
    params: {
      Jurisdiction: id
    }
  })
}

export function getAllTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}

export function getClassType() {
  return request({
    url: '/clazzType/one',
    method: 'get'
  })
}

export function getAllClassRoom() {
  return request({
    url: '/room/all',
    method: 'get'
  })
}

export function getAllstudentName() {
  return request({
    url: '/student/selectStudentNameAndAdd',
    method: 'get'
  })
}

export function getAlllesson() {
  return request({
    url: '/lesson/all',
    method: 'get'
  })
}
