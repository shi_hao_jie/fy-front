import request from '@/utils/request'
import store from '@/store/index'

export function addplans(plans) {
  const sendPlans = {
    plansClazzId: plans.plansClazzId,
    plansPostTeacherId: store.getters.userId,
    plansPrincipalTeacherId: plans.plansPeopleResponsibleTeacherId,
    plansStudentId: plans.plansStudentId,
    plansTeacherId: plans.plansPeopleTeacherId,
    plansTeachingSupervisorId: plans.plansTeachingSupervisorId
  }
  return request({
    url: '/plans/insert',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(sendPlans)
  })
}

export function addclazzplans(plans) {
  const dafds = {
    plansPeopleId: plans.plansPeopleId,
    plansclazzStageGoal: plans.plansclazzStageGoal,
    plansclazzStudentId: plans.plansclazzStudentId,
    plansclazzClazzId: plans.plansclazzClazzId,
    plansclazzTeacherId: plans.plansclazzTeacherId,
    plansclazzTrainingPrograms: plans.plansclazzTrainingPrograms,
    plansclazzTeachingPreparation: plans.plansclazzTeachingPreparation,
    plansclazzMattersNeedingAttention: plans.plansclazzMattersNeedingAttention,
    plansclazzTeachingSupervisorId: plans.plansclazzTeachingSupervisorId,
    plansclazzMedicalDirectorId: plans.plansclazzMedicalDirectorId,
    plansclazzRoomId: plans.plansclazzRoomId,
    plansclazzTime: plans.plansclazzTime,
    plansclazzPostUserId: store.getters.userId
  }
  return request({
    url: '/plansClazz/insert',
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'post',
    data: JSON.stringify(dafds)
  })
}
export function getAllplansByTeacher(BB) { // 学员计划
  return request({
    url: '/plans/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB,
      limit: 100,
      page: 1
    }
  })
}

export function getAllplans() { // 学员计划
  return request({
    url: '/plans/all',
    method: 'get',
    params: {
      limit: 100,
      page: 1
    }
  })
}

export function getAllplansA() { // 学员计划
  return request({
    url: '/plans/all',
    method: 'get'

  })
}

export function getAllclazzplans(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/TeacherIdJurisdictionEight',
    method: 'get',
    params: {
      staffId: BB,
      limit: 100,
      page: 1
    }
  })
}

export function getAllplansByDirector(BB) { // 计划教学主管
  return request({
    url: '/plans/all/TeachingDirectorJurisdictionSix',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllplansByprincipal(BB) { // 计划责任教师
  return request({
    url: '/plans/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}
export function getAllplansByMedicalID(BB) { // 计划医疗主管
  return request({
    url: '/plans/all/MedicalDirectorIdJurisdictionZero',
    method: 'get',
    params: {
      staffId: BB
    }
  })
}

export function getAllcp() { // 班级计划
  return request({
    url: '/plansClazz/all?imit=999&page=1',
    method: 'get'
  })
}

export function getAllclazzplansByMedical(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/MedicalDirectorIdJurisdictionZero?staffId=' + BB + '&limit=999&page=1',
    method: 'get'
  })
}

export function getAllclazzplansByDirector(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/TeachingDirectorJurisdictionSix?staffId=' + BB + '&limit=999&page=1',
    method: 'get'
  })
}

export function getAllclazzplansByprincipal(BB) { // 班级计划
  return request({
    url: '/plansClazz/all/principalTeacherJurisdictionSeven',
    method: 'get',
    params: {
      limit: 100,
      page: 1,
      staffId: BB
    }
  })
}

export function getAllclazzplansAAA() { // 班级计划
  return request({
    url: '/plansClazz/all?limit=999&page=1',
    method: 'get'

  })
}

export function getplansById(id) {
  return request({
    url: '/plans/one',
    method: 'get',
    params: {
      plansId: id
    }
  })
}
export function getclazzplansById(id) {
  return request({
    url: 'plansClazz/one',
    method: 'get',
    params: {
      Id: id // 根据计划id获取班级计划
    }
  })
}
export function updateplans(plans) {
  const updatePlans = {
    plansId: plans.plansId, // 计划id
    plansStudentId: plans.plansStudentId, // 学生
    plansClazzId: plans.plansClazzId, // 班级
    plansTeacherId: plans.plansPeopleTeacherId, // 授课教师
    plansPrincipalTeacherId: plans.plansPeopleResponsibleTeacherId, // 主责教师
    plansPostTeacherId: store.getters.userId,
    plansTeachingSupervisorId: plans.plansTeachingSupervisorId,
    plansMattersNeedingAttention: plans.plansMattersNeedingAttention,
    plansStageGoal: plans.plansStageGoal,
    plansTeachingPreparation: plans.plansTeachingPreparation,
    plansTrainingPrograms: plans.plansTrainingPrograms

  }
  return request({
    url: '/plans/update',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(updatePlans)
  })
}

export function updateclazzplans(plans) { // 根据班级计划id修改
  const dsfsd = {
    plansclazzStageGoal: plans.plansclazzStageGoal,
    plansclazzStudentId: plans.plansclazzStudentId,
    plansclazzClazzId: plans.plansclazzClazzId,
    plansclazzTeacherId: plans.plansclazzTeacherId,
    plansclazzTrainingPrograms: plans.plansclazzTrainingPrograms,
    plansclazzTeachingPreparation: plans.plansclazzTeachingPreparation,
    plansclazzMattersNeedingAttention: plans.plansclazzMattersNeedingAttention,
    plansclazzTeachingSupervisorId: plans.plansclazzTeachingSupervisorId,
    plansclazzMedicalDirectorId: plans.plansclazzMedicalDirectorId,
    plansclazzRoomId: plans.plansclazzRoomId,
    plansclazzTime: plans.plansclazzTime,
    plansclazzId: plans.plansclazzId
  }
  return request({
    url: '/plansClazz/update',
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'post',
    data: JSON.stringify(dsfsd)
  })
}

export function deleteplans(id) {
  return request({
    url: '/plans/',
    method: 'delete',
    params: {
      plansId: id
    }
  })
}

export function getAllTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}

export function selectroom() {
  return request({
    url: '/room/all?limit=99999&page=1',
    method: 'get'
  })
}
