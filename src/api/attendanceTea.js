import request from '@/utils/request'

export function updateAttById(attendancestaff) {
  return request({
    url: '/attendanceTea/update',
    method: 'put',
    params: {
      attTeaId: attendancestaff.attTeaId, // 考勤id
      attendance: attendancestaff.attendance, // 是否出勤 0：未出勤 1：出勤
      classId: attendancestaff.classId, // 所属班级id
      completionTimes: attendancestaff.completionTimes, // 上课次数
      reasonAbsence: attendancestaff.reasonAbsence, // 缺勤原因
      teacherId: attendancestaff.teacherId // 负责考勤老师id
    }
  })
}

export function getAllAtt() {
  return request({
    url: '/attendanceTea/all',
    method: 'get'
  })
}

export function getAttbyId(id) {
  return request({
    url: '/attendanceTea/all/all',
    method: 'get',
    params: {
      attTeaId: id
    }
  })
}
//
export function deleteAtt(id) {
  return request({
    url: '/attendanceTea/Delete',
    method: 'delete',
    params: {
      attTeaId: id
    }
  })
}

export function addAttTea(attendanceTea) {
  return request({
    url: '/attendanceTea/add',
    method: 'post',
    data: {
      attendance: attendanceTea.attendance, // 是否出勤 0：未出勤 1：出勤
      classId: attendanceTea.classId, // 所属班级id
      completionTimes: attendanceTea.completionTimes,
      reasonAbsence: attendanceTea.reasonAbsence, // 缺勤原因
      teacherId: attendanceTea.teacherId // 负责考勤老师id
    }
  })
}

// 获取学生信息//
export function getStudentString(id) {
  return request({
    url: '/student/selectStudentNameAndAddByClassId',
    method: 'get',
    params: {
      ClassId: id
    }
  })
}

// 获取教师信息//
export function getTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}

// 获取班级
export function getClassString() {
  return request({
    url: '/clazz/all',
    method: 'get'
  })
}

export function getRoom() {
  return request({
    url: '/room/all',
    method: 'get'
  })
}

export function getSearchAttAll(attendanceStaff) {
  return request({
    url: '/attendanceTea/all/all',
    method: 'get',
    params: {
      CompletionTimes: attendanceStaff.completionTimes,
      classId: attendanceStaff.classId
    }
  })
}

export function getSearchAttAllBycalssId(attendanceStaff) {
  return request({
    url: '/attendanceTea/all/all',
    method: 'get',
    params: {

      teacherId: attendanceStaff.staffId
    }
  })
}
