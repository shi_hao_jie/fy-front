import request from '@/utils/request'

export function addplan(plan) {
  return request({
    url: '/plan/one/',
    method: 'post',
    params: {
      planAccessmentinfo: plan.planAccessmentinfo,
      planAccessmenttype: plan.planAccessmenttype,
      planAreaId: plan.planAreaId,
      planExplainpeopleId: plan.planExplainpeopleId,
      planExplaintime: plan.planExplaintime,
      // planId: plan.planId,
      planPeopleId: plan.planPeopleId,
      planPhysicianleaderId: plan.planPhysicianleaderId,
      planRecoveryleaderId: plan.planRecoveryleaderId,
      planStage: plan.planStage,
      // planStudentId: plan.planStudentId,
      planSummarize: plan.planSummarize,
      planSummarizeId: plan.planSummarizeId,
      planTeacherleaderId: plan.planTeacherleaderId
      // planTime: plan.planTime

    }
  })
}

export function getAllplan() {
  return request({
    url: '/plan/all/',
    method: 'get'
  })
}

export function getplanById(id) {
  return request({
    url: '/plan/one/',
    method: 'get',
    params: {
      planId: id
    }
  })
}

export function updateplan(plan) {
  return request({
    url: '/plan/',
    method: 'put',
    params: {
      planAccessmentinfo: plan.planAccessmentinfo,
      planAccessmenttype: plan.planAccessmenttype,
      planAreaId: plan.planAreaId,
      planExplainpeopleId: plan.planExplainpeopleId,
      planExplaintime: plan.planExplaintime,
      // planId: plan.planId,
      planPeopleId: plan.planPeopleId,
      planPhysicianleaderId: plan.planPhysicianleaderId,
      planRecoveryleaderId: plan.planRecoveryleaderId,
      planStage: plan.planStage,
      // planStudentId: plan.planStudentId,
      planSummarize: plan.planSummarize,
      planSummarizeId: plan.planSummarizeId,
      planTeacherleaderId: plan.planTeacherleaderId
      // planTime: plan.planTime

    }
  })
}

export function deleteplan(id) {
  return request({
    url: '/plan/',
    method: 'delete',
    params: {
      planId: id
    }
  })
}
