import request from '@/utils/request'

export function addemergency(emergency) {
  return request({
    url: '/emergency/one',
    method: 'post',
    data: {
      emergencyClazzId: emergency.emergencyClazzId,
      emergencyClazzSkcd: emergency.emergencyClazzSkcd,
      emergencyConclusion: emergency.emergencyConclusion,
      emergencySolution: emergency.emergencySolution,
      emergencyExecute: emergency.emergencyExecute,
      emergencyStudentId: emergency.emergencyStudentId,
      emergencyStatus: emergency.emergencyStatus,
      emergencyInfo: emergency.emergencyInfo,
      emergencyTime: emergency.emergencyTime,
      emergencyTitle: emergency.emergencyTitle,
      emergencyType: emergency.emergencyType,
      emergencyStaffId: emergency.emergencyStaffId,

      emergencyMold: 0
    }
  })
}

export function addemergency2(emergency) {
  return request({
    url: '/emergency/one',
    method: 'post',
    data: {
      emergencyClazzId: emergency.emergencyClazzId,
      emergencyClazzSkcd: emergency.emergencyClazzSkcd,
      emergencyConclusion: emergency.emergencyConclusion,
      emergencySolution: emergency.emergencySolution,
      emergencyExecute: emergency.emergencyExecute,
      emergencyStudentId: emergency.emergencyStudentId,
      emergencyStatus: emergency.emergencyStatus,
      emergencyInfo: emergency.emergencyInfo,
      emergencyTime: emergency.emergencyTime,
      emergencyTitle: emergency.emergencyTitle,
      emergencyType: emergency.emergencyType,
      emergencyStaffId: emergency.emergencyStaffId,
      emergencyMold: 1
    }
  })
}

export function getAllemergency() {
  return request({
    url: '/emergency/emergencyMold',
    method: 'get',
    params: {
      emergencyMold: 0
    }
  })
}

export function getAllemergency2() {
  return request({
    url: '/emergency/emergencyMold',
    method: 'get',
    params: {
      emergencyMold: 1
    }
  })
}

export function getemergencyById(id) {
  return request({
    url: '/emergency/id',
    method: 'get',
    params: {
      emergencyId: id
    }
  })
}

export function updateemergency(emergency) {
  return request({
    url: '/emergency/',
    method: 'put',
    params: {
      emergencyId: emergency.emergencyId,
      emergencyClazzId: emergency.emergencyClazzId,
      emergencyClazzSkcd: emergency.emergencyClazzSkcd,
      emergencyConclusion: emergency.emergencyConclusion,
      emergencySolution: emergency.emergencySolution,
      emergencyExecute: emergency.emergencyExecute,
      emergencyStudentId: emergency.emergencyStudentId,
      emergencyStatus: emergency.emergencyStatus,
      emergencyInfo: emergency.emergencyInfo,
      emergencyTime: emergency.emergencyTime,
      emergencyTitle: emergency.emergencyTitle,
      emergencyType: emergency.emergencyType,
      emergencyStaffId: emergency.emergencyStaffId,
      emergencyMold: emergency.emergencyMold
    }
  })
}

export function deleteemergency(id) {
  return request({
    url: '/emergency/',
    method: 'delete',
    params: {
      emergencyId: id
    }
  })
}

export function getSearchAttAll(emergency) {
  return request({

    url: '/emergency/selectByClassIdAndSKCS',
    method: 'get',
    params: {
      clazzSkcd: emergency.clazzSkcd,
      clazzId: emergency.classId
    }
  })
}
