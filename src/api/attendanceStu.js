import request from '@/utils/request'
import store from '@/store/index'

export function updateAttById(attendanceStu) {
  return request({
    url: '/attendanceStu/',
    method: 'put',
    params: {
      attStuId: attendanceStu.attStuId, // 考勤id
      attendance: attendanceStu.attendance, // 是否出勤 0：未出勤 1：出勤
      classId: attendanceStu.classId, // 所属班级id
      reasonAbsence: attendanceStu.reasonAbsence, // 缺勤原因
      studentId: attendanceStu.studentId, // 学生id
      teacherId: attendanceStu.teacherId, // 负责考勤老师id
      abnormalSituation: attendanceStu.abnormalSituation,
      abnormalRelevantMatters: attendanceStu.abnormalRelevantMatters,
      abnormalAdult: attendanceStu.abnormalAdult,
      abnormalStatus: attendanceStu.abnormalStatus
    }
  })
}

export function adultById(adult) {
  return request({
    url: '/attendanceStu/',
    method: 'put',
    params: {
      attStuId: adult.id, // 考勤id
      abnormalSituation: adult.abnormalSituation,
      abnormalRelevantMatters: adult.abnormalRelevantMatters,
      abnormalAdult: adult.abnormalAdult,
      abnormalStatus: 1,
      abnormalStaffId: store.getters.userId
    }
  })
}

export function getAllAtt() {
  return request({
    url: '/attendanceStu/all',
    method: 'get'
  })
}

export function getAttbyId(id) {
  return request({
    url: '/attendanceStu/one',
    method: 'get',
    params: {
      attStuId: id
    }
  })
}
export function deleteAtt(id) {
  return request({
    url: '/attendanceStu/',
    method: 'delete',
    params: {
      attStuId: id
    }
  })
}

// export function addAllAtt() {
//   return request({
//     url: '/attendanceStu/all',
//     method: 'post',
//     data: {
//       classId: classId,
//       list: list
//     }
//   })
// }

export function addoneAtt(attendanceStu) {
  return request({
    url: '/attendanceStu/one',
    method: 'post',
    data: {
      attendance: attendanceStu.attendance, // 是否出勤 0：未出勤 1：出勤
      classId: attendanceStu.classId, // 所属班级id
      reasonAbsence: attendanceStu.reasonAbsence, // 缺勤原因
      studentId: attendanceStu.studentId, // 学生id
      teacherId: attendanceStu.teacherId // 负责考勤老师id
    }
  })
}

// 获取学生信息//
export function getStudentString(id) {
  return request({
    url: '/student/selectStudentNameAndAddByClassId',
    method: 'get',
    params: {
      ClassId: id
    }
  })
}

// 获取教师信息//
export function getTeacherString() {
  return request({
    url: '/staff/selectStaffnameAndStaffId',
    method: 'get'
  })
}

// 获取班级
export function getClassString() {
  return request({
    url: '/clazz/all',
    method: 'get'
  })
}

export function getRoom() {
  return request({
    url: '/room/all',
    method: 'get'
  })
}
//
export function getAllStudentString() {
  return request({
    url: '/student/selectStudentNameAndAdd',
    method: 'get'
  })
}

export function SerchByIdData(studentId) {
  return request({

    url: '/attendanceStu/selectByStudentId?studentId=' + studentId,
    method: 'get'

  })
}

export function SerchClassByIdData(attendanceStu) {
  return request({

    url: '/attendanceStu/selectStudentByClassIDAndCompletionTimes/Admin',
    method: 'get',
    params: {
      CompletionTimes: attendanceStu.completionTimes,
      classId: attendanceStu.classId
    }

  })
}
