import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
// import qs from 'qs'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    // config.data = qs.stringify(config.data)
    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    // if the custom code is not 20000, it is judged as an error.
    if (res.httpCode !== '200') {
      // 判断是否为誊录状态
      if(res.code !== '账号密码错误' && res.httpCode === '500'){
        return res
      } else{
        // 提示错误信息
        Message({
          message: res.code || '服务器繁忙，请稍后再试',
          type: 'warning',
          duration: 5 * 1000
        })
        // 判断是否为服务器异常
        if(res.httpCode === '500'){
          Message({
            message: res.code || '服务器繁忙，请稍后再试',
            type: 'warning',
            duration: 5 * 1000
          })
        } else{
          Message({
            message: res.code || '服务器繁忙，请稍后再试',
            type: 'warning',
            duration: 5 * 1000
          })
        }
        // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
        if (res.httpCode === 50008 || res.httpCode === 50012 || res.httpCode === 50014) {
          // to re-login
          MessageBox.confirm('登录超时，请您重新登录', '即将退出登录', {
            confirmButtonText: 'Re-Login',
            cancelButtonText: 'Cancel',
            type: 'warning'
          }).then(() => {
            store.dispatch('user/resetToken').then(() => {
              location.reload()
            })
          })
        }
        return Promise.reject(new Error(res.message || 'Error'))
      }
      
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: '服务器繁忙，请稍后再试',
      type: 'warning',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
