import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: '首页',
        component: () => import('@/views/dashboard/index'),
        meta: { title: '首页', icon: 'home' }
      }
    ]
  }
]

export const asyncRoutes = [
  {
    path: '/Medical',
    name: '医疗信息',
    component: Layout,
    meta: { title: '医疗信息', icon: 'staff', roles: ['信息管理', '医疗主管'] },
    children: [
      {
        path: 'info',
        component: () => import('@/views/medical/MedicalStudentinfo'),
        name: '临床信息',
        meta: { title: '临床信息', icon: 'nested', roles: ['医疗主管'] }
      },
      {
        path: 'plan',
        component: () => import('@/views/medical/MedicalPlan'),
        name: '医疗方案',
        meta: { title: '医疗方案', icon: 'nested', roles: ['医疗主管'] }
      }

    ]
  },

  {
    path: '/student',
    name: '学员管理',
    component: Layout,
    meta: { title: '学员管理', icon: 'student', roles: ['教务主任', '信息管理', '教学主任', '教学主管', '责任教师', '授课老师', '医疗主管'] },
    children: [
      {
        path: 'info',
        component: () => import('@/views/student/studentinfo'),
        name: '基本信息',
        meta: { title: '基本信息', icon: 'nested', roles: ['授课老师', '教学主管', '责任教师', '教学主任', '医疗主管'] }
      },
      {
        path: 'info',
        component: () => import('@/views/child/aStudent'),
        name: '基本信息',
        meta: { title: '基本信息', icon: 'nested', roles: ['教务主任'] }
      },
      {
        path: 'info',
        component: () => import('@/views/child/newStudent'),
        name: '基本信息',
        meta: { title: '基本信息', icon: 'nested', roles: ['中心主任', '信息管理'] }
      },

      {
        path: 'clinical',
        name: '临床信息',
        component: () => import('@/views/child/student'),
        meta: { title: '临床信息', icon: 'nested', roles: ['中心主任', '信息管理', '教学主任', '教学主管', '授课老师', '责任教师'] }
      },
      // {
      //   path: 'lesson',
      //   name: '课程信息',
      //   component: () => import('@/views/purchaseCourseRecords/noname'),
      //   meta: { title: '课程信息', icon: 'nested', roles: ['教务主任'] }
      // },
      {
        path: 'lesson',
        name: '课程信息',
        component: () => import('@/views/lesson/lesson'),
        meta: { title: '课程信息', icon: 'nested', roles: ['中心主任'] }
      },
      {
        path: 'plans',
        name: '教学计划',
        component: () => import('@/views/plans/plans'),
        meta: { title: '教学计划', icon: 'nested', roles: ['中心主任', '教务主任', '教学主任', '教学主管', '授课老师', '医疗主管', '责任教师'] }
      },

      {
        path: 'executiveCondition',
        name: '执行情况',
        component: () => import('@/views/executiveCondition/executiveCondition'),
        meta: { title: '执行情况', icon: 'nested', roles: ['中心主任'] }
      },
      {
        path: 'supervisor',
        name: '教学督导',
        component: () => import('@/views/supervisor/supervisor'),
        meta: { title: '教学督导', icon: 'nested', roles: ['中心主任'] }
      },
      {
        path: 'attendance',
        name: '出勤统计',
        component: () => import('@/views/attendanceStu/attendanceStu'),
        meta: { title: '出勤统计', icon: 'nested', roles: ['中心主任', '授课⽼师'] }
      },
      {
        path: 'backlog',
        name: '待办事件',
        component: () => import('@/views/emergency/emergency'),
        meta: { title: '待办事件', icon: 'nested', roles: ['中心主任', '授课⽼师'] }
      },
      {
        path: 'teachsummary',
        component: () => import('@/views/teachsummary/teachsummary'),
        name: '教学总结',
        meta: { title: '教学总结', icon: 'nested', roles: ['教学主任', '教学主管', '责任教师', '授课老师', '医疗主管'] }
      },
      {
        path: 'arranging',
        component: () => import('@/views/schedule/show_schedule'),
        name: '排课管理',
        meta: { title: '排课管理', icon: 'nested', roles: ['中心主任', '教务主任', '教学主任', '教学主管', '责任教师', '授课老师', '医疗主管'] }
      },
      {
        path: 'studentcomm',
        name: '学生评价',
        component: () => import('@/views/studentcomm/studentcomm'),
        meta: { title: '学生评价', icon: 'nested', roles: [] }
      }
      // {
      //   path: 'parentcomm',
      //   component: () => import('@/views/parentcomm/parentcomm'),
      //   name: '家长沟通',
      //   meta: { title: '家长沟通', icon: 'nested', roles: ['中心主任', '教学主任', '教学主管', '责任教师', '授课老师'] }
      // }
    ]
  },
  {
    path: '/class',
    name: '班级管理',
    component: Layout,
    meta: { title: '班级管理', icon: 'class', roles: ['中心主任', '教学主任', '教务主任', '教学主管', '责任教师', '授课老师', '医疗主管'] },
    children: [

      {
        path: 'info',
        component: () => import('@/views/class/class'),
        name: '班级管理',
        meta: { title: '班级管理', icon: 'class', roles: ['中心主任', '教务主任'] }
      },
      {
        path: 'clazzplans',
        name: '教学计划',
        component: () => import('@/views/plans/clazzplans'),
        meta: { title: '教学计划', icon: 'nested', roles: ['授课老师', '医疗主管', '教学主管', '责任教师', '教学主任'], keepAlive: true }
      },
      {
        path: 'lessoninfo',
        component: () => import('@/views/lesson/lessoninfo'),
        name: '课程总结',
        meta: { title: '课程总结', icon: 'nested', roles: ['教学主任', '教学主管', '责任教师', '授课老师', '医疗主管'] }
      }
      // {
      //   path: 'perform',
      //   name: '执行情况',
      //   component: () => import('@/views/executiveCondition/executiveCondition'),
      //   meta: { title: '执行情况', icon: 'nested', roles: ['中心主任'] }
      // },
      // {
      //   path: 'supervisor',
      //   name: '教学督导',
      //   component: () => import('@/views/supervisor/supervisor'),
      //   meta: { title: '教学督导', icon: 'nested', roles: ['中心主任'] }
      // },
      // {
      //   path: 'backlog',
      //   name: '待办事件',
      //   component: () => import('@/views/emergency/emergency2'),
      //   meta: { title: '待办事件', icon: 'nested', roles: ['中心主任'] }
      // }
    ]
  },
  // {
  //   path: '/academic',
  //   name: '学术管理',
  //   component: Layout,
  //   meta: { title: '学术管理', icon: 'academic', roles: ['中心主任'] },
  //   children: [
  //     {
  //       path: 'research',
  //       component: () => import('@/views/academic/academic'),
  //       name: '学术研究',
  //       meta: { title: '学术研究', icon: 'nested', roles: ['中心主任', '教务主任', '教学主任'] }
  //     },
  //     {
  //       path: 'application',
  //       name: '应用立项',
  //       component: () => import('@/views/academic/application'),
  //       meta: { title: '应用立项', icon: 'nested', roles: ['中心主任', '教务主任', '教学主任'] }
  //     },
  //     {
  //       path: 'results',
  //       name: '成果转化',
  //       component: () => import('@/views/academic/achievement'),
  //       meta: { title: '成果转化', icon: 'nested', roles: ['中心主任', '教务主任', '教学主任'] }
  //     }
  //   ]
  // },
  {
    path: '/educationalAdministration',
    name: '教学管理',
    component: Layout,
    meta: { title: '教学管理', icon: 'edu', roles: ['教学主任', '教学主管', '责任教师', '授课老师', '医疗主管'] },
    children: [
      {
        path: 'arranging',
        component: () => import('@/views/schedule/show_schedule'),
        name: '排课管理',
        meta: { title: '排课管理', icon: 'nested', roles: ['中心主任'] }
      },
      {
        path: 'discuss',
        name: '教学讨论',
        component: () => import('@/views/discussion/TeachDiscuss'),
        meta: { title: '教学讨论', icon: 'nested', roles: ['授课老师', '医疗主管', '教学主管', '责任教师', '教学主任'] }
      },
      {
        path: 'supervisor',
        name: '教学督导',
        component: () => import('@/views/supervisor/supervisor'),
        meta: { title: '教学督导', icon: 'nested', roles: ['中心主任', '教学主任', '教学主管', '责任教师', '授课老师', '医疗主管'] }
      },
      {
        path: 'supplies',
        name: '物资管理',
        component: () => import('@/views/room/room'),
        meta: { title: '物资管理', icon: 'nested', roles: ['中心主任'] }
      }
      // {
      //   path: 'emergency',
      //   name: '教学代办',
      //   component: () => import('@/views/emergency/newemergency'),
      //   meta: { title: '教学代办', icon: 'nested', roles: ['医疗主管'] }
      // }
    ]
  },
  {
    path: '/Schedule',
    name: '教务信息',
    component: Layout,
    meta: { title: '教务信息', icon: 'staff', roles: ['教务主任'] },
    children: [
      {
        path: 'teaSchedule',
        name: '教师排课表',
        component: () => import('@/views/schedule/teaSchedule'),
        meta: { title: '教师排课表', icon: 'nested', roles: ['教务主任'] }
      },
      {
        path: 'roomSchedule',
        name: '场地排课表',
        component: () => import('@/views/schedule/roomSchedule'),
        meta: { title: '场地排课表', icon: 'nested', roles: ['教务主任'] }
      },
      {
        path: 'resourcesSchedule',
        name: '资源排课表',
        component: () => import('@/views/schedule/resourcesSchedule'),
        meta: { title: '资源排课表', icon: 'nested', roles: ['教务主任'] }
      }

    ]
  },
  {
    path: '/staff',
    name: '员工管理',
    component: Layout,
    meta: { title: '员工管理', icon: 'staff', roles: ['教务主任', '信息管理'] },
    children: [
      {
        path: 'info',
        component: () => import('@/views/staff/staff'),
        name: '基本信息',
        meta: { title: '基本信息', icon: 'nested', roles: ['中心主任', '信息管理'] }
      },
      {
        path: 'infoA',
        component: () => import('@/views/staff/eduStaff'),
        name: '基本信息',
        meta: { title: '基本信息', icon: 'nested', roles: ['教务主任'] }
      },
      {
        path: 'infoB',
        component: () => import('@/views/staff/technical'),
        name: '技术信息',
        meta: { title: '技术信息', icon: 'nested', roles: ['教务主任'] }
      },
      {
        path: 'career',
        name: '职业发展',
        component: () => import('@/views/staff/career'),
        meta: { title: '职业发展', icon: 'nested', roles: ['中心主任', '信息管理'] }
      },
      {
        path: 'work',
        name: '工作情况',
        component: () => import('@/views/staff/work'),
        meta: { title: '工作情况', icon: 'nested', roles: ['中心主任', '信息管理'] }
      }
      // {
      //   path: 'attendanceTea',
      //   name: '员工考勤',
      //   component: () => import('@/views/attendanceTea/attendanceTea'),
      //   meta: { title: '员工考勤', icon: 'nested', roles: ['中心主任', '教务主任', '信息管理'] }
      // }
    ]
  },

  {
    path: '/emergency',
    name: '教学待办',
    component: Layout,
    meta: { title: '教学待办', icon: 'staff', roles: ['信息管理', '教学主管', '教学主任', '教务主任', '医疗主管'] },
    children: [
      {
        path: 'emergency',
        name: '教学待办',
        component: () => import('@/views/teachTodo/teachTodo'),
        meta: { title: '教学待办', icon: 'nested', roles: ['教务主任', '教学主任', '医疗主管', '教学主管'] }
      }

    ]
  },

  {
    path: '/financial',
    name: '财务管理',
    component: Layout,
    meta: { title: '财务管理', icon: 'financial', roles: ['财务主任'] },
    children: [
      {
        path: 'parent',
        name: '家长创建',
        component: () => import('@/views/parent/addParent'),
        meta: { title: '家长创建', icon: 'nested', roles: ['财务主任'] }
      },
      {
        path: 'pay',
        name: '缴费',
        component: () => import('@/views/pay/pay'),
        meta: { title: '缴费', icon: 'nested', roles: ['财务主任'] }
      },
      {
        path: 'cost',
        component: () => import('@/views/pay/payList'),
        name: '费用统计',
        meta: { title: '费用统计', icon: 'money', roles: ['中心主任', '财务主任'] }
      }
      // {
      //   path: 'service',
      //   name: '服务统计',
      //   component: () => import('@/views/financial/service'),
      //   meta: { title: '服务统计', icon: 'nested', roles: ['中心主任', '财务主任'] }
      // },
      // {
      //   path: 'business',
      //   name: '经营统计',
      //   component: () => import('@/views/financial/business'),
      //   meta: { title: '经营统计', icon: 'nested', roles: ['中心主任', '财务主任'] }
      // }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
