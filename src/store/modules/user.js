import { login, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'
import { Message } from 'element-ui'

const getDefaultState = () => {
  return {
    token: getToken(),
    userId: '',
    name: '',
    avatar: '',
    roles: []
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_USER_ID: (state, userId) => {
    state.userId = userId
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    var that=this
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        if(response.httpCode === '200' || response.code === '0'){
          const data = response
          commit('SET_TOKEN', data.data)
          setToken(data.data)
        }else{
          Message({
            message: response.code || 'Error',
            type: 'warning',
            duration: 5 * 1000
          })
        }
        resolve()
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const data = response
        if (!data) {
          return reject('Verification failed, please Login again.')
        }
        const roles = []
        // const { roles, name, avatar, introduction } = data
        switch (data.data.data.staffJurisdiction) {
          case 0:roles.push('医疗主管'); break
          case 1:roles.push('中心主任'); break
          case 2:roles.push('教务主任'); break
          case 3:roles.push('信息管理'); break
          case 4:roles.push('财务主任'); break
          case 5:roles.push('教学主任'); break
          case 6:roles.push('教学主管'); break
          case 7:roles.push('责任教师'); break
          case 8:roles.push('授课老师'); break
        }
        // roles.push(data.data.staffJurisdiction)
        // roles must be a non-empty array
        if (!roles || roles.length <= 0) {
          reject('getInfo: roles must be a non-null array!')
        }
        commit('SET_ROLES', roles)
        commit('SET_NAME', data.data.data.staffName)
        commit('SET_USER_ID', data.data.data.staffId)
        commit('SET_AVATAR', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif')
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      // logout(state.token).then(() => {
      removeToken() // must remove  token  first
      resetRouter()
      commit('RESET_STATE')
      resolve()
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

